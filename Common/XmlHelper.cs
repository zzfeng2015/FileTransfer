using System;
using System.Xml;

namespace Common
{
	/// <summary>
	/// LoadXML 的摘要说明。
	/// </summary>
    public class XmlHelper
    {
        #region 局部变量
        private string filePath = "";
        private XmlDocument doc = null;
        #endregion

        #region 构造函数
        /// <summary>
        /// 构造函数(参数)
        /// </summary>
        /// <param name="XMLFilePath">XML文档路径</param>
        public XmlHelper(string XMLFilePath)
        {
            this.filePath = XMLFilePath;
            this.doc = this.LoadXMLFile();
        }
        #endregion

        #region 加载XML文档
        /// <summary>
        /// 读取XML文件
        /// </summary>
        /// <returns>加载的XML文档</returns>
        public XmlDocument LoadXMLFile()
        {
            XmlDocument docTmp = null;
            try
            {
                if (this.filePath != "")
                {
                    docTmp = new XmlDocument();
                    docTmp.Load(this.filePath);
                }
            }
            catch
            {
                docTmp = null;
            }
            return docTmp;
        }

        /// <summary>
        /// 读取XML文件
        /// </summary>
        /// <returns>加载的XML文档</returns>
        public void LoadXml()
        {
            try
            {
                this.doc.Load(this.filePath);
            }
            catch
            {

            }
        }

        /// <summary>
        /// 读取指定的XML文件
        /// </summary>
        /// <param name="XMLFilePath">XML文档路径</param>
        /// <returns>加载的XML文档</returns>
        public XmlDocument LoadXMLFile(string XMLFilePath)
        {
            XmlDocument docTmp = null;
            try
            {
                if (this.filePath != "")
                {
                    this.filePath = XMLFilePath;
                    docTmp = new XmlDocument();
                    docTmp.Load(this.filePath);
                }
            }
            catch
            {
                docTmp = null;
            }
            return docTmp;
        }
        #endregion

        #region 判断指定节点是否存在
        /// <summary>
        /// 判断指定节点是否存在
        /// </summary>
        /// <param name="xmlFile">XML文档路径</param>
        /// <param name="parentNodeName">父节点名称（非空带"/"）</param>
        /// <param name="childNodeName">子节点Key名称</param>
        /// <returns></returns>
        public static bool CheckNodeExist(string xmlFile, string parentNodeName, string childNodeName)
        {
            bool res = false;
            try
            {
                if (xmlFile != "" && childNodeName != "")
                {
                    XmlDocument docT = new XmlDocument();
                    docT.Load(xmlFile);
                    XmlNode node = docT.SelectSingleNode(@"//SettingItems/" + parentNodeName + "ItemData[@name='" + childNodeName + "']");
                    if (node != null)
                        res = true;
                }
            }
            catch { }
            return res;
        }
        #endregion

        #region 获取属性
        /// <summary>
        /// 根据Key值获取属性
        /// </summary>
        /// <param name="strKey">Key</param>
        /// <returns>Value</returns>
        public string getValue(string strNodeName)
        {
            string strRes = "";
            try
            {
                if (strNodeName != "")
                {
                    //this.doc = this.LoadXMLFile();
                    if (this.doc != null)
                    {
                        XmlNode node = doc.SelectSingleNode(@"//SettingItems/ItemData[@name='" + strNodeName + "']");
                        if (node != null)
                        {
                            XmlAttribute xa = node.Attributes["value"];
                            if (xa != null)
                                strRes = xa.Value;
                        }
                    }
                }
            }
            catch
            {
            }
            return strRes;
        }

        /// <summary>
        /// 根据Key获取属性值（单节结构）
        /// </summary>
        /// <param name="xmlFile">XML文档</param>
        /// <param name="strNodeName">Key名称</param>
        /// <returns></returns>
        public static string getValue(string xmlFile, string strNodeName)
        {
            string strRes = "";
            try
            {
                if (xmlFile != "" && strNodeName != "")
                {
                    XmlDocument docT = new XmlDocument();
                    docT.Load(xmlFile);
                    XmlNode node = docT.SelectSingleNode(@"//SettingItems/ItemData[@name='" + strNodeName + "']");

                    if (node != null)
                    {
                        XmlAttribute xa = node.Attributes["value"];
                        if (xa != null)
                            strRes = xa.Value;
                    }
                }
            }
            catch
            {
            }
            return strRes;
        }

        /// <summary>
        /// 根据Key获取属性值(多节结构)
        /// </summary>
        /// <param name="xmlFile">XML文档</param>
        /// <param name="parentNodeName">父节点名称(带"/")</param>
        /// <param name="strNodeName">Key名称</param>
        /// <returns></returns>
        public static string getValue(string xmlFile, string parentNodeName, string strNodeName)
        {
            string strRes = "";
            try
            {
                if (xmlFile != "" && strNodeName != "")
                {
                    XmlDocument docT = new XmlDocument();
            
                        docT.Load(xmlFile);
                        XmlNode node = docT.SelectSingleNode(@"//SettingItems/" + parentNodeName + "ItemData[@name='" + strNodeName + "']");

                        if (node != null)
                        {
                            XmlAttribute xa = node.Attributes["value"];
                            if (xa != null)
                                strRes = xa.Value;
                        }
                }
            }
            catch
            {
            }
            return strRes;
        }
        #endregion

        #region 设置属性
        public void setValue(string strNodeName, string strValue)
        {
            try
            {
                if (strNodeName != "")
                {
                    //this.doc = this.LoadXMLFile();
                    if (this.doc != null)
                    {
                        XmlNode node = doc.SelectSingleNode(@"//SettingItems/ItemData[@name='" + strNodeName + "']");
                        if (node != null)
                        {
                                XmlAttribute xa = node.Attributes["value"];
                                if (xa != null)
                                    xa.Value = strValue;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// 设置属性值（单节）
        /// </summary>
        /// <param name="xmlFile">XML文件</param>
        /// <param name="strNodeName">节点名称</param>
        /// <param name="strValue">节点值</param>
        public static void setValue(string xmlFile, string strNodeName, string strValue)
        {
            try
            {
                if (xmlFile != "" && strNodeName != "")
                {
                    XmlDocument docT = new XmlDocument();
                        docT.Load(xmlFile);

                        XmlNode node = docT.SelectSingleNode(@"//SettingItems/ItemData[@name='" + strNodeName + "']");
                        if (node != null)
                        {
                            XmlAttribute xa = node.Attributes["value"];
                            if (xa != null)
                                xa.Value = strValue;
                        }
                        else
                        {
                            XmlElement xe = null;
                            xe = docT.CreateElement("ItemData");
                            xe.SetAttribute("name", strNodeName);
                            xe.SetAttribute("value", strValue);
                            docT.SelectSingleNode(@"//SettingItems").AppendChild((XmlNode)xe);
                        }
                        docT.Save(xmlFile);
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// 设置属性值（多节）
        /// </summary>
        /// <param name="xmlFile">XML文件</param>
        /// <param name="parentNodeName">父节点名称(带"/")</param>
        /// <param name="strNodeName">节点名称</param>
        /// <param name="strValue">节点值</param>
        public static void setValue(string xmlFile, string parentNodeName, string strNodeName, string strValue)
        {
            try
            {
                if (xmlFile != "" && strNodeName != "")
                {
                    XmlDocument docT = new XmlDocument();

                        docT.Load(xmlFile);
                        XmlNode node = docT.SelectSingleNode(@"//SettingItems/" + parentNodeName + "ItemData[@name='" + strNodeName + "']");

                        if (node != null)
                        {
                            XmlAttribute xa = node.Attributes["value"];
                            if (xa != null)
                                xa.Value = strValue;
                        }
                        else
                        {
                            XmlElement xe = docT.CreateElement("ItemData");
                            xe.SetAttribute("name", strNodeName);
                            xe.SetAttribute("value", strValue);

                            node = docT.SelectSingleNode(@"//SettingItems/" + parentNodeName.TrimEnd('/'));
                            if (node == null)
                            {
                                XmlElement xElement = docT.CreateElement(parentNodeName.TrimEnd('/'));
                                docT.SelectSingleNode(@"//SettingItems").AppendChild((XmlNode)xElement);
                            }
                            docT.SelectSingleNode(@"//SettingItems/" + parentNodeName.TrimEnd('/')).AppendChild((XmlNode)xe);
                        }
                        docT.Save(xmlFile);
                }
            }
            catch
            {
            }
        }
        #endregion

        #region 保存XML文档
        /// <summary>
        /// 保存XML文档
        /// </summary>
        public void SaveXMLFile()
        {
            try
            {
                //this.doc = this.LoadXMLFile();
                if (this.doc != null)
                {
                    lock (doc)
                    {
                        this.doc.Save(this.filePath);
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// 保存指定的XML文档
        /// </summary>
        /// <param name="XMLFilePath">XML文档</param>
        public void SaveXMLFile(string XMLFilePath)
        {
            try
            {
                //this.doc = this.LoadXMLFile(XMLFilePath);
                if (this.doc != null)
                    this.doc.Save(XMLFilePath);
            }
            catch
            {
            }
        }
        #endregion

        #region 新增/删除
        /// <summary>
        /// 新增节点
        /// </summary>
        /// <param name="strNodeName">节点属性名称</param>
        /// <param name="strValue">节点属性值</param>
        public void AddNode(string strNodeName, string strValue)
        {
            try
            {
                if (strNodeName != "")
                {
                    if (this.doc != null)
                    {
                        XmlElement xe = null;
                        xe = this.doc.CreateElement("ItemData");
                        xe.SetAttribute("name", strNodeName);
                        xe.SetAttribute("value", strValue);
                        lock (this.doc)
                        {
                            doc.SelectSingleNode(@"//SettingItems").AppendChild((XmlNode)xe);
                        }
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// 新增节点(单节)
        /// </summary>
        /// <param name="xmlFile">XML文档</param>
        /// <param name="strNodeName">节点属性名称</param>
        /// <param name="strValue">节点属性值</param>
        public static void AddNode(string xmlFile, string strNodeName, string strValue)
        {
            try
            {
                if (xmlFile != "" && strNodeName != "")
                {
                    XmlDocument docT = new XmlDocument();

                        docT.Load(xmlFile);
                        XmlNode node = docT.SelectSingleNode(@"//SettingItems/ItemData[@name='" + strNodeName + "']");
                        if (node != null)
                        {
                            XmlAttribute xa = node.Attributes["value"];
                            if (xa != null)
                                xa.Value = strValue;
                        }
                        else
                        {
                            XmlElement xe = null;
                            xe = docT.CreateElement("ItemData");
                            xe.SetAttribute("name", strNodeName);
                            xe.SetAttribute("value", strValue);
                            docT.SelectSingleNode(@"//SettingItems").AppendChild((XmlNode)xe);
                        }
                        docT.Save(xmlFile);

                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// 新增节点(多节)
        /// </summary>
        /// <param name="xmlFile">XML文档</param>
        /// <param name="parentNodeName">父节点名称（名称前带"/"）</param>
        /// <param name="strNodeName">节点属性名称</param>
        /// <param name="strValue">节点属性值</param>
        public static void AddNode(string xmlFile, string parentNodeName, string strNodeName, string strValue)
        {
            try
            {
                if (xmlFile != "" && strNodeName != "")
                {
                    XmlDocument docT = new XmlDocument();

                        docT.Load(xmlFile);
                        XmlNode node = docT.SelectSingleNode(@"//SettingItems" + parentNodeName + "/ItemData[@name='" + strNodeName + "']");

                        if (node != null)
                        {
                            XmlAttribute xa = node.Attributes["value"];
                            if (xa != null)
                                xa.Value = strValue;
                        }
                        else
                        {
                            XmlElement xe = docT.CreateElement("ItemData");
                            xe.SetAttribute("name", strNodeName);
                            xe.SetAttribute("value", strValue);

                            node = docT.SelectSingleNode(@"//SettingItems" + parentNodeName.TrimEnd('/'));
                            if (node == null)
                            {
                                XmlElement xElement = docT.CreateElement(parentNodeName.Trim('/'));
                                docT.SelectSingleNode(@"//SettingItems").AppendChild((XmlNode)xElement);
                            }
                            docT.SelectSingleNode(@"//SettingItems" + parentNodeName).AppendChild((XmlNode)xe);
                        }
                        docT.Save(xmlFile);
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// 删除节点
        /// </summary>
        /// <param name="strNodeName">待删除节点属性名称</param>
        public void DelNode(string strNodeName)
        {
            try
            {
                if (strNodeName != "")
                {
                    if (this.doc != null)
                    {
                        XmlNode node = doc.SelectSingleNode(@"//SettingItems/ItemData[@name='" + strNodeName + "']");
                        if (node != null)
                        {
                            lock (this.doc)
                            {
                                this.doc.SelectSingleNode(@"//SettingItems").RemoveChild(node);
                            }
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// 删除节点(单节)
        /// </summary>
        /// <param name="xmlFile">XML文档</param>
        /// <param name="strNodeName">待删除节点属性名称</param>
        public static void DelNode(string xmlFile, string strNodeName)
        {
            try
            {
                if (xmlFile != "" && strNodeName != "")
                {
                    XmlDocument docT = new XmlDocument();
                        docT.Load(xmlFile);
                        XmlNode node = docT.SelectSingleNode(@"//SettingItems/ItemData[@name='" + strNodeName + "']");
                        if (node != null)
                        {
                            docT.SelectSingleNode(@"//SettingItems").RemoveChild(node);
                            docT.Save(xmlFile);
                        }
                }
            }
            catch { }
        }

        /// <summary>
        /// 删除节点(多节)
        /// </summary>
        /// <param name="xmlFile">XML文档</param>
        /// <param name="parentNodeName">父节点名称(名称前带"/")</param>
        /// <param name="strNodeName">待删除节点属性名称</param>
        public static void DelNode(string xmlFile, string parentNodeName, string strNodeName)
        {
            try
            {
                if (xmlFile != "" && strNodeName != "")
                {
                    XmlDocument docT = new XmlDocument();
                        docT.Load(xmlFile);
                        XmlNode node = docT.SelectSingleNode(@"//SettingItems" + parentNodeName + "/ItemData[@name='" + strNodeName + "']");
                        if (node != null)
                        {
                            docT.SelectSingleNode(@"//SettingItems" + parentNodeName).RemoveChild(node);
                            docT.Save(xmlFile);
                        }
                }
            }
            catch { }
        }
        #endregion

        #region 获取节点对值
        /// <summary>
        /// 获取节点对值
        /// </summary>
        /// <param name="xmlFile">XML文档</param>
        /// <param name="parentNodeName">父节点名称</param>
        /// <returns>name:value|name:value...</returns>
        public static string GetKeyValues(string xmlFile, string parentNodeName, string attributeName, string attributeValue)
        {
            string res = "";
            XmlNode node = null;
            string strName = "";
            string strValue = "";
            try
            {
                if (xmlFile != "" && parentNodeName != "" && attributeName != "" && attributeValue != "")
                {
                    XmlDocument docT = new XmlDocument();
                        docT.Load(xmlFile);
                        XmlNodeList nodeList = docT.SelectNodes(@"//SettingItems/" + parentNodeName + "/ItemData");
                        for (int i = 0; nodeList != null && i < nodeList.Count; i++)
                        {
                            node = nodeList[i];
                            if (node != null)
                            {
                                strName = node.Attributes[attributeName].Value;
                                strValue = node.Attributes[attributeValue].Value;
                                if (res == "")
                                    res = strName + "?" + strValue;
                                else
                                    res += "|" + strName + "?" + strValue;
                            }
                        }
                }
            }
            catch { }
            return res;
        }

        /// <summary>
        /// 新增节点对值
        /// </summary>
        /// <param name="xmlFile"></param>
        /// <param name="parentNodeName"></param>
        /// <param name="attributeName"></param>
        /// <param name="attributeValue"></param>
        /// <returns></returns>
        public static void AddKeyValues(string xmlFile, string parentNodeName, string attributeName, string attributeValue, string keys, string values)
        {
            XmlNode node = null;
            XmlElement xe = null;
            try
            {
                if (xmlFile != "" && parentNodeName != "" && attributeName != "" && keys != "")
                {
                    XmlDocument docT = new XmlDocument();
                        docT.Load(xmlFile);
                        node = docT.SelectSingleNode(@"//SettingItems/" + parentNodeName);
                        if (node != null)
                        {
                            node = docT.SelectSingleNode(@"//SettingItems/" + parentNodeName + "/ItemData[@" + attributeName + "='" + keys + "']");
                            if (node != null)
                            {
                                node.Attributes[attributeValue].Value = values;
                            }
                            else
                            {
                                xe = docT.CreateElement("ItemData");
                                xe.SetAttribute(attributeName, keys);
                                xe.SetAttribute(attributeValue, values);
                                docT.SelectSingleNode(@"//SettingItems/" + parentNodeName).AppendChild((XmlNode)xe);
                                docT.Save(xmlFile);
                            }
                        }
                        else
                        {
                            xe = docT.CreateElement(parentNodeName);
                            XmlElement xeTmp = docT.CreateElement("ItemData");
                            xeTmp.SetAttribute(attributeName, keys);
                            xeTmp.SetAttribute(attributeValue, values);
                            xe.AppendChild(xeTmp);
                            docT.SelectSingleNode(@"//SettingItems").AppendChild((XmlNode)xe);
                            docT.Save(xmlFile);
                        }
                }
            }
            catch { }
        }

        /// <summary>
        /// 删除节点对值
        /// </summary>
        /// <param name="xmlFile"></param>
        /// <param name="parentNodeName"></param>
        /// <param name="attributeName"></param>
        /// <param name="attributeValue"></param>
        /// <returns></returns>
        public static void DelKeyValues(string xmlFile, string parentNodeName, string keyName, string keyValue)
        {
            XmlNode node = null;
            try
            {
                if (xmlFile != "" && parentNodeName != "" && keyName != "" && keyValue != "")
                {
                    XmlDocument docT = new XmlDocument();
                        docT.Load(xmlFile);
                        node = docT.SelectSingleNode(@"//SettingItems/" + parentNodeName);
                        if (node != null)
                        {
                            node = docT.SelectSingleNode(@"//SettingItems/" + parentNodeName + "/ItemData[@" + keyName + "='" + keyValue + "']");
                            if (node != null)
                            {
                                docT.SelectSingleNode(@"//SettingItems/" + parentNodeName).RemoveChild(node);
                                docT.Save(xmlFile);
                            }
                        }
                }
            }
            catch { }
        }

        /// <summary>
        /// 获取节点对值
        /// </summary>
        /// <param name="xmlFile">XML文档</param>
        /// <param name="parentNodeName">父节点名称</param>
        /// <returns>name:value|name:value...</returns>
        public static string GetKeyValues(string xmlFile, string parentNodeName, string keyName, string keyValue, string attributeName1, string splitStr, string attributeName2)
        {
            string res = "";
            XmlNode node = null;
            string strValue1 = "";
            string strValue2 = "";
            try
            {
                if (xmlFile != "" && parentNodeName != "" && keyName != "" && keyValue != "" && attributeName1 != "" && attributeName2 != "")
                {
                    XmlDocument docT = new XmlDocument();
                        docT.Load(xmlFile);
                        node = docT.SelectSingleNode(@"//SettingItems/" + parentNodeName + "/ItemData[@" + keyName + "='" + keyValue + "']");
                        if (node != null)
                        {
                            strValue1 = node.Attributes[attributeName1].Value;
                            strValue2 = node.Attributes[attributeName2].Value;
                            res = strValue1 + splitStr + strValue2;
                        }
                }
            }
            catch { }
            return res;
        }

        /// <summary>
        /// 新增节点对值
        /// </summary>
        /// <param name="xmlFile"></param>
        /// <param name="parentNodeName"></param>
        /// <param name="attributeName"></param>
        /// <param name="attributeValue"></param>
        /// <returns></returns>
        public static void AddKeyValues(string xmlFile, string parentNodeName, string keyName, string keyValue, string name1, string value1, string name2, string value2)
        {
            XmlNode node = null;
            XmlElement xe = null;
            try
            {
                if (xmlFile != "" && parentNodeName != "" && keyName != "" && keyValue != "" && name1 != "" && name2 != "")
                {
                    XmlDocument docT = new XmlDocument();
                        docT.Load(xmlFile);
                        node = docT.SelectSingleNode(@"//SettingItems/" + parentNodeName);
                        if (node != null)
                        {
                            node = docT.SelectSingleNode(@"//SettingItems/" + parentNodeName + "/ItemData[@" + keyName + "='" + keyValue + "']");
                            if (node != null)
                            {
                                node.Attributes[name1].Value = value1;
                                node.Attributes[name2].Value = value2;
                            }
                            else
                            {
                                xe = docT.CreateElement("ItemData");
                                xe.SetAttribute(keyName, keyValue);
                                xe.SetAttribute(name1, value1);
                                xe.SetAttribute(name2, value2);
                                docT.SelectSingleNode(@"//SettingItems/" + parentNodeName).AppendChild((XmlNode)xe);
                                docT.Save(xmlFile);
                            }
                        }
                        else
                        {
                            xe = docT.CreateElement(parentNodeName);
                            XmlElement xeTmp = docT.CreateElement("ItemData");
                            xe.SetAttribute(keyName, keyValue);
                            xe.SetAttribute(name1, value1);
                            xe.SetAttribute(name2, value2);
                            xe.AppendChild(xeTmp);
                            docT.SelectSingleNode(@"//SettingItems").AppendChild((XmlNode)xe);
                            docT.Save(xmlFile);
                        }
                }
            }
            catch { }
        }
        #endregion
    }
}
