﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

namespace Common
{
    public class Email
    {
        private string strServer;
        /// <summary>
        /// SMTP服务器
        /// </summary>
        public string Server
        {
            get
            {
                return strServer;
            }
            set
            {
               strServer = value;
            }
        }

        private int strPort = 25;
        /// <summary>
        /// SMTP服务器端口
        /// </summary>
        public int Port
        {
            get
            {
                return strPort;
            }
            set
            {
                strPort = value;
            }
        }

        private string strUser = null;
        /// <summary>
        /// 发件人账户
        /// </summary>
        public String SendUser
        {
            get
            {
                return strUser;
            }
            set
            {
                strUser = value;
            }
        }

        private string strUserName = null;
        /// <summary>
        /// 发件人名称
        /// </summary>
        public String SendUserName
        {
            get
            {
                return strUserName;
            }
            set
            {
                strUserName = value;
            }
        }

        private string strPass = null;
        /// <summary>
        /// 发件人账户密码
        /// </summary>
        public String Password
        {
            get
            {
                return strPass;
            }
            set
            {
                strPass = value;
            }
        }

        private string strSub = "";
        /// <summary>
        /// 邮件主题
        /// </summary>
        public String Subject
        {
            get
            {
                return strSub;
            }
            set
            {
                strSub = value;
            }
        }

        private string strBody = "";
        /// <summary>
        /// 邮件正文
        /// </summary>
        public String Body
        {
            get
            {
                return strBody;
            }
            set
            {
                strBody = value;
            }
        }

        private string[] toAddrs = null;
        /// <summary>
        /// 收件人地址列表
        /// </summary>
        public string[] ToAddrs
        {
            set { this.toAddrs = value; }
            get { return this.toAddrs; }
        }

        private string encodingCode = "GBK";
        /// <summary>
        /// 邮件统一编码
        /// </summary>
        public string EncodingCode
        {
            set { this.encodingCode = value; }
            get { return this.encodingCode; }
        }

        private string[] attachments = null;
        /// <summary>
        /// 附件列表(附件地址)
        /// </summary>
        public string[] Attachments
        {
            set { this.attachments = value; }
            get { return this.attachments; }
        }

        /// <summary>
        /// 邮件发送
        /// </summary>
        public void SendMail()
        {
            SmtpClient client = null;
            System.Net.Mail.MailMessage msg = null;

            try
            {
                using (msg = new System.Net.Mail.MailMessage())
                {
                    foreach (string addr in this.toAddrs)
                    {
                        msg.To.Add(addr);
                    }

                    foreach (string att in this.attachments)
                    {
                        Attachment item = new Attachment(att);
                        item.Name = System.IO.Path.GetFileName(att);
                        //item.ContentDisposition.FileName = System.IO.Path.GetFileName(att);
                        item.NameEncoding = System.Text.Encoding.GetEncoding(encodingCode);
                        item.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                        msg.Attachments.Add(item);
                    }

                    msg.From = new MailAddress(this.strUser, this.strUserName, System.Text.Encoding.GetEncoding(encodingCode)); //发件人地址，发件人姓名，编码
                    msg.Subject = this.strSub;//邮件标题 
                    msg.SubjectEncoding = System.Text.Encoding.GetEncoding(encodingCode);//邮件标题编码 
                    msg.BodyEncoding = System.Text.Encoding.GetEncoding(encodingCode);//邮件内容编码 
                    msg.IsBodyHtml = true;//是否是HTML邮件 
                    msg.Priority = MailPriority.Normal;//邮件优先级 
                    msg.Body = this.strBody;//邮件内容 

                    
                
                        client = new SmtpClient();
                        //client.Timeout = 0;//系统默认 100,000毫秒(100秒)
                        client.Credentials = new System.Net.NetworkCredential(this.strUser, this.strPass); //邮箱和密码 
                        client.Host = this.Server; //邮件服务器
                        client.Port = this.strPort;
                        client.Send(msg);
                  
                }
            }
            catch (Exception ex)
            {
                if (client != null)
                    client = null;
                if (msg != null)
                    msg.Dispose();
                throw ex;
            }
        }
    }
}
