﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Config
{
    /// <summary>
    /// 系统缓存基类
    /// </summary>
    public class SystemCache
    {
        private static Hashtable cache = Hashtable.Synchronized(new Hashtable());//线程安全

        #region 继承方法
        /// <summary>
        /// 不过期缓存键是否存在
        /// </summary>
        /// <param name="keyName">缓存键名称</param>
        public static bool ConfigCacheContainsKey(string keyName)
        {
            if (cache.ContainsKey(keyName))
            {
                if (((CacheValueModel)cache[keyName]).ExpiryTime < DateTime.Now)//缓存过期
                {
                    cache.Remove(keyName);
                    return false;
                }
                else
                    return true;
            }
            else
                return false;
        }

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="cacheName">缓存名称</param>
        /// <param name="cacheValue">缓存值</param>
        public static void AddCache(string cacheName,object cacheValue)
        {
            AddCache(cacheName, cacheValue, 0);
        }

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="cacheName">缓存名称</param>
        /// <param name="cacheValue">缓存值</param>
        /// <param name="cacheMinute">缓存分钟数</param>
        public static void AddCache(string cacheName, object cacheValue, int cacheMinute)
        {
            lock (cache.SyncRoot)
            {
                if (cache.ContainsKey(cacheName))
                    cache.Remove(cacheName);

                CacheValueModel cvm = new CacheValueModel();
                cvm.CacheName = cacheName;
                cvm.CacheValue = cacheValue;

                if (cacheMinute > 0)//指定缓存时间
                    cvm.ExpiryTime = DateTime.Now.AddMinutes(cacheMinute);
                else//缓存不过期
                    cvm.ExpiryTime = DateTime.MaxValue;
                cache.Add(cacheName, cvm);
            }
        }

        /// <summary>
        /// 获取缓存值
        /// </summary>
        /// <param name="cacheName">缓存名称</param>
        /// <returns>缓存值</returns>
        public static object GetCacheValue(string cacheName)
        {
            if (cache.ContainsKey(cacheName))
                return ((CacheValueModel)cache[cacheName]).CacheValue;
            else
                return null;
        }

        /// <summary>
        /// 获取bool缓存值 1为true 其他为false
        /// </summary>
        /// <param name="cacheName">缓存名称</param>
        /// <returns>缓存值</returns>
        public static bool GetBoolCacheValue(string cacheName)
        {
            if (cache[cacheName] != null)
                return ((CacheValueModel)cache[cacheName]).CacheValue.ToString() == "1" ? true : false;
            else
                return false;
        }

        /// <summary>
        /// 获取string缓存值
        /// </summary>
        /// <param name="cacheName">缓存名称</param>
        /// <returns>缓存值</returns>
        public static string GetStringCacheValue(string cacheName)
        {
            if (cache[cacheName] != null)
                return ((CacheValueModel)cache[cacheName]).CacheValue.ToString();
            else
                return string.Empty;
        }

        /// <summary>
        /// 获取int缓存值
        /// </summary>
        /// <param name="cacheName">缓存名称</param>
        /// <returns>缓存值</returns>
        public static Int32 GetIntCacheValue(string cacheName)
        {
            if (cache[cacheName] != null)
                return Convert.ToInt32(((CacheValueModel)cache[cacheName]).CacheValue);
            else
                return 0;
        }

        /// <summary>
        /// 清空缓存
        /// </summary>
        public static void ClearCache()
        {
            if (cache != null)
                cache.Clear();
        }

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="cacheName">缓存名称</param>
        public static void DelCache(string cacheName)
        {
            if (cache.ContainsKey(cacheName))
                cache.Remove(cacheName);
        }
        #endregion

        #region 公共方法
        /// <summary>
        /// 不过期缓存键是否存在
        /// </summary>
        /// <param name="hashCache">线程安全的Hashtable缓存对象</param>
        /// <param name="keyName">缓存键名称</param>
        public static bool CacheContainsUnexpiryTimeKey(Hashtable hashCache,string keyName)
        {
            if (hashCache != null && hashCache.ContainsKey(keyName))
            {
                if (((CacheValueModel)hashCache[keyName]).ExpiryTime < DateTime.Now)//缓存过期
                {
                    lock (hashCache.SyncRoot)
                    {
                        hashCache.Remove(keyName);
                    }
                    return false;
                }
                else
                    return true;
            }
            else
                return false;
        }

        /// <summary>
        /// 直接获取不过期缓存值
        /// </summary>
        /// <param name="hashCache">线程安全的Hashtable缓存对象</param>
        /// <param name="keyName">缓存键名称</param>
        /// <returns>缓存值，为空不存在或已过期</returns>
        public static object CacheContainsUnexpiryTimeKeyValue(Hashtable hashCache, string keyName)
        {
            object res = null;
            CacheValueModel cvm = null;

            if (hashCache != null && hashCache.ContainsKey(keyName))
            {
                cvm = (CacheValueModel)hashCache[keyName];
                if (cvm.ExpiryTime < DateTime.Now)//缓存过期
                {
                    lock (hashCache.SyncRoot)
                    {
                        hashCache.Remove(keyName);
                    }
                }
                else
                    res = cvm.CacheValue;
            }

            return res;
        }

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="hashCache">线程安全的Hashtable缓存对象</param>
        /// <param name="cacheName">缓存名称</param>
        /// <param name="cacheValue">缓存值</param>
        /// <param name="cacheMinute">缓存分钟数 0为持久缓存</param>
        public static void AddCache(Hashtable hashCache,string cacheName, object cacheValue, int cacheMinute)
        {
            if (hashCache != null)
            {
                lock (hashCache.SyncRoot)
                {
                    if (hashCache.ContainsKey(cacheName))
                        hashCache.Remove(cacheName);

                    CacheValueModel cvm = new CacheValueModel();
                    cvm.CacheName = cacheName;
                    cvm.CacheValue = cacheValue;

                    if (cacheMinute > 0)//指定缓存时间
                        cvm.ExpiryTime = DateTime.Now.AddMinutes(cacheMinute);
                    else//缓存不过期
                        cvm.ExpiryTime = DateTime.MaxValue;
                    hashCache.Add(cacheName, cvm);
                }
            }
        }

        /// <summary>
        /// 获取缓存值
        /// </summary>
        /// <param name="hashCache">线程安全的Hashtable缓存对象</param>
        /// <param name="cacheName">缓存名称</param>
        /// <returns>缓存值</returns>
        public static object GetObjectCacheValue(Hashtable hashCache, string cacheName)
        {
            if (hashCache != null && hashCache.ContainsKey(cacheName))
                return ((CacheValueModel)hashCache[cacheName]).CacheValue;
            else
                return null;
        }

        /// <summary>
        /// 获取bool缓存值 1为true 其他为false
        /// </summary>
        /// <param name="hashCache">线程安全的Hashtable缓存对象</param>
        /// <param name="cacheName">缓存名称</param>
        /// <returns>缓存值</returns>
        public static bool GetBoolCacheValue(Hashtable hashCache, string cacheName)
        {
            if (hashCache != null && hashCache[cacheName] != null)
                return ((CacheValueModel)hashCache[cacheName]).CacheValue.ToString() == "1" ? true : false;
            else
                return false;
        }

        /// <summary>
        /// 获取string缓存值
        /// </summary>
        /// <param name="hashCache">线程安全的Hashtable缓存对象</param>
        /// <param name="cacheName">缓存名称</param>
        /// <returns>缓存值</returns>
        public static string GetStringCacheValue(Hashtable hashCache, string cacheName)
        {
            if (hashCache != null && hashCache[cacheName] != null)
                return ((CacheValueModel)hashCache[cacheName]).CacheValue.ToString();
            else
                return string.Empty;
        }

        /// <summary>
        /// 获取int缓存值
        /// </summary>
        /// <param name="hashCache">线程安全的Hashtable缓存对象</param>
        /// <param name="cacheName">缓存名称</param>
        /// <returns>缓存值</returns>
        public static Int32 GetIntCacheValue(Hashtable hashCache, string cacheName)
        {
            if (hashCache != null && hashCache[cacheName] != null)
                return Convert.ToInt32(((CacheValueModel)hashCache[cacheName]).CacheValue);
            else
                return 0;
        }

        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <param name="hashCache">线程安全的Hashtable缓存对象</param>
        public static void ClearCache(Hashtable hashCache)
        {
            if (hashCache != null)
                hashCache.Clear();
        }

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="hashCache">线程安全的Hashtable缓存对象</param>
        /// <param name="cacheName">缓存名称</param>
        public static void DelCache(Hashtable hashCache,string cacheName)
        {
            if (hashCache != null && hashCache.ContainsKey(cacheName))
                hashCache.Remove(cacheName);
        }
        #endregion
    }

    /// <summary>
    /// 系统缓存模型
    /// </summary>
    public class CacheValueModel
    {
        private string _cacheName;
        /// <summary>
        /// 缓存名称
        /// </summary>
        public string CacheName
        {
            get { return this._cacheName; }
            set { _cacheName = value; }
        }

        private object _cacheValue;
        /// <summary>
        /// 缓存值
        /// </summary>
        public object CacheValue
        {
            get { return this._cacheValue; }
            set { _cacheValue = value; }
        }

        private DateTime _expiryTime;
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpiryTime
        {
            get { return this._expiryTime; }
            set { _expiryTime = value; }
        }
    }
}
