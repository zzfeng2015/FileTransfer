using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using Common;
using System.IO;
using System.Xml;

namespace Config
{
    /// <summary>
    /// 系统设置类
    /// </summary>
    public class SystemSet
    {
        private static Hashtable _cache = Hashtable.Synchronized(new Hashtable());//线程安全
        /// <summary>
        /// 清空缓存
        /// </summary>
        public static void ClearCache()
        {
            if (_cache != null)
                _cache.Clear();
        }
        /// <summary>
        /// 系统配置文件
        /// </summary>
        public const string SYSTEM_CONFIGXML = "FTConfig.xml";

        #region 系统信息
        /// <summary>
        /// 应用程序名称
        /// </summary>
        public static string AppName
        {
            get
            {
                return "FileTransfer";
            }
        }

        /// <summary>
        /// 服务标识名称
        /// </summary>
        public static string AppServiceName
        {
            get
            {
                string str = "";
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "AppServiceName"))
                    str = SystemCache.GetStringCacheValue(_cache, "AppServiceName");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), SYSTEM_CONFIGXML), "AppServiceName");
                    SystemCache.AddCache(_cache, "AppServiceName", str, SystemConfigCacheTime);
                }
                return str;
            }
            set 
            {
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "AppServiceName", value); 
            }
        }

        /// <summary>
        /// 服务描述
        /// </summary>
        public static string AppServiceDesc
        {
            get
            {
                string str = "";
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "AppServiceDesc"))
                    str = SystemCache.GetStringCacheValue(_cache, "AppServiceDesc");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), SYSTEM_CONFIGXML), "AppServiceDesc");
                    SystemCache.AddCache(_cache, "AppServiceDesc", str, SystemConfigCacheTime);
                }
                return str;
            }
            set
            {
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "AppServiceDesc", value);
            }
        }

        /// <summary>
        /// FTP主动模式指定本地IP地址(IPV4,当本地有多个网络时使用)
        /// </summary>
        public static string ActiveFtpLocalIp
        {
            set { XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "ActiveFtpLocalIp", value); }
            get
            {
                string str = "";
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "ActiveFtpLocalIp"))
                    str = SystemCache.GetStringCacheValue(_cache, "ActiveFtpLocalIp");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "ActiveFtpLocalIp");
                    SystemCache.AddCache(_cache, "ActiveFtpLocalIp", str, SystemConfigCacheTime);
                }
                return str;
            }
        }

        /// <summary>
        /// 系统配置信息默认缓存时间（单位：分钟，0为永久缓存）
        /// </summary>
        public static int SystemConfigCacheTime
        {
            get
            {
                int res = 30;
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "SystemConfigCacheTime"))
                    res = SystemCache.GetIntCacheValue(_cache, "SystemConfigCacheTime");
                else
                {
                    try
                    {
                        res = Convert.ToInt32(XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "SystemConfigCacheTime"));
                    }
                    catch { }
                    SystemCache.AddCache(_cache, "SystemConfigCacheTime", res, res);
                }
                return res;
            }
        }

        /// <summary>
        /// 每次获取数量
        /// </summary>
        public static int GetAmountOneTime
        {
            get
            {
                int res = 500;
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "GetAmountOneTime"))
                    res = SystemCache.GetIntCacheValue(_cache, "GetAmountOneTime");
                else
                {
                    try
                    {
                        res = Convert.ToInt32(XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetAmountOneTime"));
                    }
                    catch { }
                    SystemCache.AddCache(_cache, "GetAmountOneTime", res, SystemConfigCacheTime);
                }
                return res;
            }
        }

        /// <summary>
        /// 每次上传数量
        /// </summary>
        public static int UploadAmountOneTime
        {
            get
            {
                int res = 500;
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "UploadAmountOneTime"))
                    res = SystemCache.GetIntCacheValue(_cache, "UploadAmountOneTime");
                else
                {
                    try
                    {
                        res = Convert.ToInt32(XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadAmountOneTime"));
                    }
                    catch { }
                    SystemCache.AddCache(_cache, "UploadAmountOneTime", res, SystemConfigCacheTime);
                }
                return res;
            }
        }

        /// <summary>
        /// 下载后临时存放本地目录
        /// </summary>
        /// <returns></returns>
        public static string TmpDownMsgPath
        {
            set { XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,SYSTEM_CONFIGXML), "TmpDown", value); }
            get { 
                string str = "";
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "TmpDown"))
                    str = SystemCache.GetStringCacheValue(_cache, "TmpDown");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "TmpDown");
                    SystemCache.AddCache(_cache, "TmpDown", str, SystemConfigCacheTime);
                }
                return str;
            }
        }

        /// <summary>
        /// 临时存放本地待上传目录
        /// </summary>
        /// <returns></returns>
        public static string TmpUpMsgPath
        {
            set { XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,SYSTEM_CONFIGXML), "TmpUp", value); }
            get {
                string str = "";
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "TmpUp"))
                    str = SystemCache.GetStringCacheValue(_cache, "TmpUp");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "TmpUp");
                    SystemCache.AddCache(_cache, "TmpUp", str, SystemConfigCacheTime);
                }
                return str;
            }
        }

        /// <summary>
        /// 日志文件目录
        /// </summary>
        /// <returns></returns>
        public static string LogPath
        {
            set { XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "LogFile", value); }
            get { 
                string str = "";
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "LogFile"))
                    str = SystemCache.GetStringCacheValue(_cache, "LogFile");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "LogFile");
                    SystemCache.AddCache(_cache, "LogFile", str, SystemConfigCacheTime);
                }
                return str;
            }
        }

        /// <summary>
        /// 保存目录(顶层目录)
        /// </summary>
        /// <returns></returns>
        public static string LocalSuccessPath
        {
            set { XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,SYSTEM_CONFIGXML), "LocalSuccess", value); }
            get { 
                string str = "";
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "LocalSuccess"))
                    str = SystemCache.GetStringCacheValue(_cache, "LocalSuccess");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "LocalSuccess");
                    SystemCache.AddCache(_cache, "LocalSuccess", str, SystemConfigCacheTime);
                }
                return str;
            }
        }
        #endregion

        #region 获取控制
        public static bool CheckTaskNo(string taskNo, string taskNos)
        {
            bool res = false;

            if (string.IsNullOrEmpty(taskNo))
                return false;

            string[] nos = taskNos.Split(',');
            for (int i = 0; nos != null && i < nos.Length; i++)
            {
                if (nos[i] != null && nos[i].ToUpper() == taskNo.ToUpper())
                {
                    res = true;
                    break;
                }
            }
            return res;
        }

        /// <summary>
        /// 获取任务代码
        /// </summary>
        public static string GetTaskNos
        {
            get { 
                string str = "";
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "GetTaskNos"))
                    str = SystemCache.GetStringCacheValue(_cache, "GetTaskNos");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetTaskNos");
                    SystemCache.AddCache(_cache, "GetTaskNos", str, SystemConfigCacheTime);
                }
                return str;
            }
            set { XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetTaskNos", value); }
        }
        public static void RemoveGetTaskNos(string taskNo)
        {
            string res = "";
            if (string.IsNullOrEmpty(taskNo))
                return;

            string[] nos = GetTaskNos.Split(',');
            for (int i = 0; nos != null && i < nos.Length; i++)
            {
                if (!string.IsNullOrEmpty(nos[i]) && nos[i].ToUpper() != taskNo.ToUpper())
                {
                    if (res == "")
                        res = nos[i];
                    else
                        res = res + "," + nos[i];
                }
            }
            GetTaskNos = res;
        }

        /// <summary>
        /// 新增任务代码
        /// </summary>
        public static void AddTaskNo(string taskNo, string getThreadSet, string getThreadTime, string getMode, string savePath, string saveTimes)
        {
            XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetThread", taskNo.ToUpper(), getThreadSet);
            XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetThreadTime", taskNo.ToUpper(), getThreadTime);
            XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetMode", taskNo.ToUpper(), getMode);
            XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetMsgSaveRoot", taskNo.ToUpper(), savePath);
            XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetMsgSaveTimes", taskNo.ToUpper(), saveTimes);
        }
        /// <summary>
        /// 设置企业类型
        /// </summary>
        public static void SetTaskNo(string taskNo, string getThreadSet, string getThreadTime, string getMode, string savePath, string saveTimes)
        {
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetThread/", taskNo.ToUpper(), getThreadSet);
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetThreadTime/", taskNo.ToUpper(), getThreadTime);
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetMode/", taskNo.ToUpper(), getMode);
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetMsgSaveRoot/", taskNo.ToUpper(), savePath);
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetMsgSaveTimes/", taskNo.ToUpper(), saveTimes);
        }
        /// <summary>
        /// 删除任务代码
        /// </summary>
        public static void DelTaskNo(string taskNo)
        {
            XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetThread", taskNo.ToUpper());
            XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetThreadTime", taskNo.ToUpper());
            XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetMode", taskNo.ToUpper());
            XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetMsgSaveRoot", taskNo.ToUpper());
            XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetMsgSaveTimes", taskNo.ToUpper());

            DelMsgGetRoot(taskNo);
            DelFtp(taskNo, false);
            DelEmail(taskNo, false);
            DelMsmq(taskNo, false);
            DelActiveMq(taskNo, false);

            RemoveGetTaskNos(taskNo);
        }

        public static void AddMsgGetRoot(string taskNo, string msgGetRoot, string msgGetFilter)
        {
            XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/MsgGetRoot", taskNo.ToUpper(), msgGetRoot);
            XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/MsgGetRootFilter", taskNo.ToUpper(), msgGetFilter);
        }
        public static void SetMsgGetRoot(string taskNo, string msgGetRoot, string msgGetFilter)
        {
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "MsgGetRoot/", taskNo.ToUpper(), msgGetRoot);
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "MsgGetRootFilter/", taskNo.ToUpper(), msgGetFilter);
        }
        public static void DelMsgGetRoot(string taskNo)
        {
            XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/MsgGetRoot", taskNo.ToUpper());
            XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/MsgGetRootFilter", taskNo.ToUpper());
        }

        /// <summary>
        /// 获取状态设置
        /// </summary>
        public static string GetGetThreadSet(string taskNo)
        {
            return XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetThread/", taskNo.ToUpper());
        }
        /// <summary>
        /// 设置获取状态
        /// </summary>
        public static void SetGetThreadSet(string taskNo,string status)
        {
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetThread/", taskNo.ToUpper(), status);
        }

        /// <summary>
        /// 获取线程间隔时间
        /// </summary>
        public static int GetGetThreadTime(string taskNo)
        {
            int res = 5;
            if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":GetThreadTime"))
                res = SystemCache.GetIntCacheValue(_cache, taskNo + ":GetThreadTime");
            else
            {
                try
                {
                    res = Convert.ToInt32(XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetThreadTime/", taskNo.ToUpper()));
                }
                catch { }
                SystemCache.AddCache(_cache, taskNo + ":GetThreadTime", res, SystemConfigCacheTime);
            }
            return res;
        }

        /// <summary>
        /// 获取企业获取方式
        /// </summary>
        public static string GetGetMsgMode(string taskNo)
        {
            string str = "";
            if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":GetMode"))
                str = SystemCache.GetStringCacheValue(_cache, taskNo + ":GetMode");
            else
            {
                str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,SYSTEM_CONFIGXML), "GetMode/", taskNo.ToUpper());
                SystemCache.AddCache(_cache, taskNo + ":GetMode", str, SystemConfigCacheTime);
            }
            return str;
        }

        /// <summary>
        /// 获取企业本地保存目录
        /// </summary>
        public static string GetGetMsgSavePath(string taskNo)
        {
            string str = "";
            if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":GetMsgSaveRoot"))
                str = SystemCache.GetStringCacheValue(_cache, taskNo + ":GetMsgSaveRoot");
            else
            {
                str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetMsgSaveRoot/", taskNo.ToUpper());
                SystemCache.AddCache(_cache, taskNo + ":GetMsgSaveRoot", str, SystemConfigCacheTime);
            }
            return str;
        }

        /// <summary>
        /// 获取企业本地保存目录
        /// </summary>
        public static Int32 GetGetMsgSaveTimes(string taskNo)
        {

            int res = 1;
            if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":GetMsgSaveTimes"))
                res = SystemCache.GetIntCacheValue(_cache, taskNo + ":GetMsgSaveTimes");
            else
            {
                try
                {
                    res = Convert.ToInt32(XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetMsgSaveTimes/", taskNo.ToUpper()));
                }
                catch { }
                SystemCache.AddCache(_cache, taskNo + ":GetMsgSaveTimes", res, SystemConfigCacheTime);
            }
            return res;
        }

        /// <summary>
        /// 获取本地获取目录
        /// </summary>
        public static string GetGetMsgRoot(string taskNo)
        {
            string str = "";
            if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":MsgGetRoot"))
                str = SystemCache.GetStringCacheValue(_cache, taskNo + ":MsgGetRoot");
            else
            {
                str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "MsgGetRoot/", taskNo.ToUpper());
                SystemCache.AddCache(_cache, taskNo + ":MsgGetRoot", str, SystemConfigCacheTime);
            }
            return str;
        }

        /// <summary>
        /// 获取本地文件匹配符
        /// </summary>
        public static string GetGetMsgFilter(string taskNo)
        {
            string str = "";
            if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":MsgGetRootFilter"))
                str = SystemCache.GetStringCacheValue(_cache, taskNo + ":MsgGetRootFilter");
            else
            {
                str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "MsgGetRootFilter/", taskNo.ToUpper());
                SystemCache.AddCache(_cache, taskNo + ":MsgGetRootFilter", str, SystemConfigCacheTime);
            }
            return str;
        }
        #endregion

        #region 上传控制
        /// <summary>
        /// 上传任务代码
        /// </summary>
        public static string UploadTaskNos
        {
            get { 
                string str = "";
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "UploadTaskNos"))
                    str = SystemCache.GetStringCacheValue(_cache, "UploadTaskNos");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadTaskNos");
                    SystemCache.AddCache(_cache, "UploadTaskNos", str, SystemConfigCacheTime);
                }
                return str;
            }
            set { XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadTaskNos", value); }
        }
        public static void RemoveUploadTaskNos(string taskNo)
        {
            string res = "";
            if (string.IsNullOrEmpty(taskNo))
                return;

            string[] corps = UploadTaskNos.Split(',');
            for (int i = 0; corps != null && i < corps.Length; i++)
            {
                if (!string.IsNullOrEmpty(corps[i]) && corps[i].ToUpper() != taskNo.ToUpper())
                {
                    if (res == "")
                        res = corps[i];
                    else
                        res = res + "," + corps[i];
                }
            }
            UploadTaskNos = res;
        }

        public static void AddUploadTaskNo(string taskNo, string threadSet, string threadTime, string mode)
        {
            XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadThread", taskNo.ToUpper(), threadSet);
            XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadThreadTime", taskNo.ToUpper(), threadTime);
            XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadMode", taskNo.ToUpper(), mode);
        }
        public static void SetUploadTaskNo(string taskNo, string threadSet, string threadTime, string mode)
        {
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadThread/", taskNo.ToUpper(), threadSet);
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadThreadTime/", taskNo.ToUpper(), threadTime);
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadMode/", taskNo.ToUpper(), mode);
        }
        public static void DelUploadTaskNo(string taskNo)
        {
            XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadThread", taskNo.ToUpper());
            XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadThreadTime", taskNo.ToUpper());
            XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadMode", taskNo.ToUpper());

            DelUploadLoaclRoot(taskNo);
            DelFtp(taskNo, true);
            DelEmail(taskNo, true);
            DelMsmq(taskNo, true);
            DelActiveMq(taskNo, true);

            RemoveUploadTaskNos(taskNo);
        }

        public static void AddUploadLocalRoot(string taskNo, string root, string filter)
        {
            XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/LocalUploadRoot", taskNo.ToUpper(), root);
            XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/LocalUploadRootFilter", taskNo.ToUpper(), filter);
        }
        public static void SetUploadLocalRoot(string taskNo, string root, string filter)
        {
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "LocalUploadRoot/", taskNo.ToUpper(), root);
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "LocalUploadRootFilter/", taskNo.ToUpper(), filter);
        }
        public static void DelUploadLoaclRoot(string taskNo)
        {
            XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/LocalUploadRoot", taskNo.ToUpper());
            XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/LocalUploadRootFilter", taskNo.ToUpper());
        }

        /// <summary>
        /// 获取上传状态设置
        /// </summary>
        public static string GetUploadThreadSet(string taskNo)
        {
            return XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadThread/", taskNo.ToUpper());
        }

        /// <summary>
        /// 设置企业上传状态
        /// </summary>
        public static void SetUploadThreadSet(string taskNo,string status)
        {
            XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadThread/", taskNo.ToUpper(), status);
        }

        /// <summary>
        /// 获取上传线程间隔时间
        /// </summary>
        public static int GetUploadThreadSetTime(string taskNo)
        {
            int res = 5;
            if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadThreadTime"))
                res = SystemCache.GetIntCacheValue(_cache, taskNo + ":UploadThreadTime");
            else
            {
                try
                {
                    res = Convert.ToInt32(XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadThreadTime/", taskNo.ToUpper()));
                }
                catch { }
                SystemCache.AddCache(_cache, taskNo + ":UploadThreadTime", res, SystemConfigCacheTime);
            }
            return res;
        }

        /// <summary>
        /// 获取本地上传目录
        /// </summary>
        public static string GetUploadLoaclRoot(string taskNo)
        {
            string str = "";
            if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":LocalUploadRoot"))
                str = SystemCache.GetStringCacheValue(_cache, taskNo + ":LocalUploadRoot");
            else
            {
                str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "LocalUploadRoot/", taskNo.ToUpper());
                SystemCache.AddCache(_cache, taskNo + ":LocalUploadRoot", str, SystemConfigCacheTime);
            }
            return str;
        }

        /// <summary>
        /// 获取本地上传文件匹配符
        /// </summary>
        public static string GetUploadLoaclRootFilter(string taskNo)
        {
            string str = "";
            if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":LocalUploadRootFilter"))
                str = SystemCache.GetStringCacheValue(_cache, taskNo + ":LocalUploadRootFilter");
            else
            {
                str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "LocalUploadRootFilter/", taskNo.ToUpper());
                SystemCache.AddCache(_cache, taskNo + ":LocalUploadRootFilter", str, SystemConfigCacheTime);
            }
            return str;
        }

        /// <summary>
        /// 获取上传方式
        /// </summary>
        public static string GetUoloadMsgMode(string taskNo)
        {
            string str = "";
            if(SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadMode"))
                str = SystemCache.GetStringCacheValue(_cache, taskNo + ":UploadMode");
            else
            {
                str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadMode/", taskNo.ToUpper());
                SystemCache.AddCache(_cache, taskNo + ":UploadMode", str, SystemConfigCacheTime);
            }
            return str;
        }
        #endregion

        #region Email
        /// <summary>
        /// 邮件接收超时的时间
        /// </summary>
        public static int MailTimeOut
        {
            get
            {
                int res = 3600;
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "MailTimeOut"))
                    res = SystemCache.GetIntCacheValue(_cache, "MailTimeOut");
                else
                {
                    try
                    {
                        res = Convert.ToInt32(XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "MailTimeOut"));
                    }
                    catch { }
                    SystemCache.AddCache(_cache, "MailTimeOut", res, SystemConfigCacheTime);
                }
                return res;
            }
        }

        /// <summary>
        /// Email接收后是否删除
        /// </summary>
        public static bool MailDelete
        {
            get
            {
                string str = "";
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "MailDelete"))
                    return SystemCache.GetBoolCacheValue(_cache, "MailDelete");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "MailDelete");
                    SystemCache.AddCache(_cache, "MailDelete", str, SystemConfigCacheTime);
                    return (str == "1" ? true : false);
                }
            }
        }

        /// <summary>
        /// Email接收附件后文件名是否加时间：0，否；1，是
        /// </summary>
        public static bool MailAttFileNameAddDate
        {
            get
            {
                string str = "";
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "MailAttFileNameAddDate"))
                    return SystemCache.GetBoolCacheValue(_cache, "MailAttFileNameAddDate");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "MailAttFileNameAddDate");
                    SystemCache.AddCache(_cache, "MailAttFileNameAddDate", str, SystemConfigCacheTime);
                    return (str == "1" ? true : false);
                }
            }
        }

        public static void AddEmail(string taskNo, string server, string port, string user, string pass, string to, bool isUpload)
        {
            if (!isUpload)
            {
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/EmailServer", taskNo.ToUpper(), server);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/EmailPort", taskNo.ToUpper(), port);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/EmailUser", taskNo.ToUpper(), user);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/EmailUserPass", taskNo.ToUpper(), pass);
            }
            else
            {
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadEmailServer", taskNo.ToUpper(), server);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadEmailPort", taskNo.ToUpper(), port);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadEmailUser", taskNo.ToUpper(), user);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadEmailUserPass", taskNo.ToUpper(), pass);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadEmailTo", taskNo.ToUpper(), to);
            }
        }

        public static void SetEmail(string taskNo, string server, string port, string user, string pass, string to, bool isUpload)
        {
            if (!isUpload)
            {
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "EmailServer/", taskNo.ToUpper(), server);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "EmailPort/", taskNo.ToUpper(), port);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "EmailUser/", taskNo.ToUpper(), user);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "EmailUserPass/", taskNo.ToUpper(), pass);
            }
            else
            {
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadEmailServer/", taskNo.ToUpper(), server);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadEmailPort/", taskNo.ToUpper(), port);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadEmailUser/", taskNo.ToUpper(), user);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadEmailUserPass/", taskNo.ToUpper(), pass);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadEmailTo/", taskNo.ToUpper(), to);
            }
        }

        public static void DelEmail(string taskNo, bool isUpload)
        {
            if (!isUpload)
            {
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/EmailServer", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/EmailPort", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/EmailUser", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/EmailUserPass", taskNo.ToUpper());
            }
            else
            {
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadEmailServer", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadEmailPort", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadEmailUser", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadEmailUserPass", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadEmailTo", taskNo.ToUpper());
            }
        }

        /// <summary>
        /// 企业邮件地址
        /// </summary>
        public static string GetEmailServer(string taskNo,bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":EmailServer"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":EmailServer");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "EmailServer/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":EmailServer", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadEmailServer"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":UploadEmailServer");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadEmailServer/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":UploadEmailServer", str, SystemConfigCacheTime);
                }
            }
            return str;
        }

        /// <summary>
        /// 企业邮件端口
        /// </summary>
        public static int GetEmailPort(string taskNo, bool isUpload)
        {
            int res = 0;
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":EmailPort"))
                    res = SystemCache.GetIntCacheValue(_cache, taskNo + ":EmailPort");
                else
                {
                    try
                    {
                        res = Convert.ToInt32(XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "EmailPort/", taskNo.ToUpper()));
                    }
                    catch { res = 110; }
                    SystemCache.AddCache(_cache, taskNo + ":EmailPort", res, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadEmailPort"))
                    res = SystemCache.GetIntCacheValue(_cache, taskNo + ":UploadEmailPort");
                else
                {
                    try
                    {
                        res = Convert.ToInt32(XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadEmailPort/", taskNo.ToUpper()));
                    }
                    catch { res = 25; }
                    SystemCache.AddCache(_cache, taskNo + ":UploadEmailPort", res, SystemConfigCacheTime);
                }
            }
            return res;
        }

        /// <summary>
        /// 企业邮箱用户(完整邮箱地址)
        /// </summary>
        public static string GetEmailUser(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":EmailUser"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":EmailUser");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "EmailUser/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":EmailUser", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadEmailUser"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":UploadEmailUser");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadEmailUser/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":UploadEmailUser", str, SystemConfigCacheTime);
                }
            }
            return str;
        }

        /// <summary>
        /// 企业邮箱用户密码
        /// </summary>
        public static string GetEmailUserPass(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":EmailUserPass"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":EmailUserPass");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "EmailUserPass/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":EmailUserPass", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadEmailUserPass"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":UploadEmailUserPass");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadEmailUserPass/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":UploadEmailUserPass", str, SystemConfigCacheTime);
                }
            }
            return str;
        }

        public static string GetEmailTo(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":EmailTo"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":EmailTo");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "EmailTo/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":EmailTo", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadEmailTo"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":UploadEmailTo");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadEmailTo/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":UploadEmailTo", str, SystemConfigCacheTime);
                }
            }
            return str;
        }
        #endregion

        #region Ftp
        /// <summary>
        /// 删除FTP已下载
        /// </summary>
        public static bool IsDelete
        {
            get
            {
                string str = "";
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "IsDelete"))
                    return SystemCache.GetBoolCacheValue(_cache, "IsDelete");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "IsDelete");
                    SystemCache.AddCache(_cache, "IsDelete", str, SystemConfigCacheTime);
                    return (str == "1" ? true : false);
                }
            }
        }

        /// <summary>
        /// 备份FTP已上传
        /// </summary>
        public static bool IsUpBackup
        {
            get
            {
                string str = "";
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, "IsUpBackup"))
                    return SystemCache.GetBoolCacheValue(_cache, "IsUpBackup");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "IsUpBackup");
                    SystemCache.AddCache(_cache, "IsUpBackup", str, SystemConfigCacheTime);
                    return (str == "1" ? true : false);
                }
            }
        }

        public static void AddFtp(string taskNo, string server, string port, string user, string pass, string root, string ftpMode, string ftpFileEx, string ftpFileRename, string ftpEncoding, bool isUpload)
        {
            if (!isUpload)
            {
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpServer", taskNo.ToUpper(), server);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpPort", taskNo.ToUpper(), port);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpUser", taskNo.ToUpper(), user);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpUserPass", taskNo.ToUpper(), pass);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpRoot", taskNo.ToUpper(), root);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpMode", taskNo.ToUpper(), ftpMode);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpFileEx", taskNo.ToUpper(), ftpFileEx);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpFileRename", taskNo.ToUpper(), ftpFileRename);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpEncoding", taskNo.ToUpper(), ftpEncoding);
            }
            else
            {
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpServer", taskNo.ToUpper(), server);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpPort", taskNo.ToUpper(), port);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpUser", taskNo.ToUpper(), user);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpUserPass", taskNo.ToUpper(), pass);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpRoot", taskNo.ToUpper(), root);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpMode", taskNo.ToUpper(), ftpMode);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpEncoding", taskNo.ToUpper(), ftpEncoding);
            }
        }

        public static void SetFtp(string taskNo, string server, string port, string user, string pass, string root, string ftpMode, string ftpFileEx, string ftpFileRename, string ftpEncoding, bool isUpload)
        {
            if (!isUpload)
            {
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpServer/", taskNo.ToUpper(), server);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpPort/", taskNo.ToUpper(), port);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpUser/", taskNo.ToUpper(), user);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpUserPass/", taskNo.ToUpper(), pass);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpRoot/", taskNo.ToUpper(), root);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpMode/", taskNo.ToUpper(), ftpMode);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpFileEx/", taskNo.ToUpper(), ftpFileEx);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpFileRename/", taskNo.ToUpper(), ftpFileRename);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpEncoding/", taskNo.ToUpper(), ftpEncoding);
            }
            else
            {
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpServer/", taskNo.ToUpper(), server);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpPort/", taskNo.ToUpper(), port);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpUser/", taskNo.ToUpper(), user);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpUserPass/", taskNo.ToUpper(), pass);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpRoot/", taskNo.ToUpper(), root);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpMode/", taskNo.ToUpper(), ftpMode);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpEncoding/", taskNo.ToUpper(), ftpEncoding);
            }
        }

        public static void DelFtp(string taskNo, bool isUpload)
        {
            if (!isUpload)
            {
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpServer", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpPort", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpUser", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpUserPass", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpRoot", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpMode", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpFileEx", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpFileRename", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/DownFtpEncoding", taskNo.ToUpper());
            }
            else
            {
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpServer", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpPort", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpUser", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpUserPass", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpRoot", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpMode", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/UploadFtpEncoding", taskNo.ToUpper());
            }
        }

        /// <summary>
        /// 获取FTP下载服务器地址
        /// </summary>
        public static string GetFtpServer(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":DownFtpServer"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":DownFtpServer");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpServer/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":DownFtpServer", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadFtpServer"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":UploadFtpServer");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpServer/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":UploadFtpServer", str, SystemConfigCacheTime);
                }
            }
            return str;
        }

        /// <summary>
        /// 获取FTP端口
        /// </summary>
        public static int GetFtpPort(string taskNo, bool isUpload)
        {
            int res = 21;
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":DownFtpPort"))
                    res = SystemCache.GetIntCacheValue(_cache, taskNo + ":DownFtpPort");
                else
                {
                    try
                    {
                        res = Convert.ToInt32(XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpPort/", taskNo.ToUpper()));
                    }
                    catch {}
                    SystemCache.AddCache(_cache, taskNo + ":DownFtpPort", res, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadFtpPort"))
                    res = SystemCache.GetIntCacheValue(_cache, taskNo + ":UploadFtpPort");
                else
                {
                    try
                    {
                        res = Convert.ToInt32(XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpPort/", taskNo.ToUpper()));
                    }
                    catch {}
                    SystemCache.AddCache(_cache, taskNo + ":UploadFtpPort", res, SystemConfigCacheTime);
                }
            }
            return res;
        }

        /// <summary>
        /// 获取FTP下载服务器用户
        /// </summary>
        public static string GetFtpUser(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":DownFtpUser"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":DownFtpUser");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpUser/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":DownFtpUser", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadFtpUser"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":UploadFtpUser");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpUser/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":UploadFtpUser", str, SystemConfigCacheTime);
                }
            }
            return str;
        }

        /// <summary>
        /// 获取FTP下载服务器用户密码
        /// </summary>
        public static string GetFtpUserPass(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":DownFtpUserPass"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":DownFtpUserPass");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpUserPass/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":DownFtpUserPass", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadFtpUserPass"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":UploadFtpUserPass");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpUserPass/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":UploadFtpUserPass", str, SystemConfigCacheTime);
                }
            }
            return str;
        }

        /// <summary>
        /// 获取FTP下载服务器根目录
        /// </summary>
        public static string GetFtpRoot(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":DownFtpRoot"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":DownFtpRoot");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpRoot/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":DownFtpRoot", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadFtpRoot"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":UploadFtpRoot");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpRoot/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":UploadFtpRoot", str, SystemConfigCacheTime);
                }
            }
            return str;
        }

        /// <summary>
        /// 获取FTP服务器模式：False,被动模式；True,主动模式
        /// </summary>
        public static bool GetFtpMode(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":DownFtpMode"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":DownFtpMode");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpMode/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":DownFtpMode", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadFtpMode"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":UploadFtpMode");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpMode/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":UploadFtpMode", str, SystemConfigCacheTime);
                }
            }
            if (str == "1")
                return true;
            else
                return false;
        }

        /// <summary>
        /// 获取FTP服务器编码
        /// </summary>
        /// <param name="corpNo"></param>
        /// <returns></returns>
        public static string GetFtpEncoding(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":DownFtpEncoding"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":DownFtpEncoding");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpEncoding/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":DownFtpEncoding", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":UploadFtpEncoding"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":UploadFtpEncoding");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "UploadFtpEncoding/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":UploadFtpEncoding", str, SystemConfigCacheTime);
                }
            }
            return str;
        }

        /// <summary>
        /// 获取FTP下载文件匹配
        /// </summary>
        public static string GetFtpDownFileEx(string taskNo)
        {
            string str = "";
            if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":DownFtpFileEx"))
                str = SystemCache.GetStringCacheValue(_cache, taskNo + ":DownFtpFileEx");
            else
            {
                str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpFileEx/", taskNo.ToUpper());
                SystemCache.AddCache(_cache, taskNo + ":DownFtpFileEx", str, SystemConfigCacheTime);
            }
            if (string.IsNullOrEmpty(str))
                str = "*.*";
            return str;
        }

        /// <summary>
        /// 获取FTP下载文件前重命名(默认重命名)
        /// </summary>
        public static bool GetFtpDownFileRename(string taskNo)
        {
            string str = "";
            if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":DownFtpFileRename"))
                str = SystemCache.GetStringCacheValue(_cache, taskNo + ":DownFtpFileRename");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "DownFtpFileRename/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":DownFtpFileRename", str, SystemConfigCacheTime);
                }
            if (str == "1" || string.IsNullOrEmpty(str))
                return true;
            else 
                return false;
        }
        #endregion

        #region MSMQ
        /// <summary>
        /// 新增MSMQ消息队列任务
        /// </summary>
        public static void AddMsmq(string taskNo, string queueName, string transaction,string txtFile,string encoding, bool isUpload)
        {
            if (!isUpload)
            {
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetQueueName", taskNo.ToUpper(), queueName);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetQueueTransaction", taskNo.ToUpper(), transaction);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetQueueTxtFile", taskNo.ToUpper(), txtFile);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetQueueFileEncoding", taskNo.ToUpper(), encoding);
            }
            else
            {
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/SendQueueName", taskNo.ToUpper(), queueName);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/SendQueueTransaction", taskNo.ToUpper(), transaction);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/SendQueueTxtFile", taskNo.ToUpper(), txtFile);
                XmlHelper.AddNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/SendQueueFileEncoding", taskNo.ToUpper(), encoding);
            }
        }

        /// <summary>
        ///设置MSMQ消息队列任务
        /// </summary>
        public static void SetMsmq(string taskNo, string queueName, string transaction, string txtFile, string encoding, bool isUpload)
        {
            if (!isUpload)
            {
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetQueueName/", taskNo.ToUpper(), queueName);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetQueueTransaction/", taskNo.ToUpper(), transaction);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetQueueTxtFile/", taskNo.ToUpper(), txtFile);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetQueueFileEncoding/", taskNo.ToUpper(), encoding);
            }
            else
            {
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "SendQueueName/", taskNo.ToUpper(), queueName);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "SendQueueTransaction/", taskNo.ToUpper(), transaction);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "SendQueueTxtFile/", taskNo.ToUpper(), txtFile);
                XmlHelper.setValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "SendQueueFileEncoding/", taskNo.ToUpper(), encoding);
            }
        }

        /// <summary>
        /// 删除MSMQ设置
        /// </summary>
        public static void DelMsmq(string taskNo, bool isUpload)
        {
            if (!isUpload)
            {
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetQueueName", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetQueueTransaction", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetQueueTxtFile", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetQueueFileEncoding", taskNo.ToUpper());
            }
            else
            {
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/SendQueueName", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/SendQueueTransaction", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/SendQueueTxtFile", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/SendQueueFileEncoding", taskNo.ToUpper());
            }
        }

        /// <summary>
        /// 获取MSMQ队列名称
        /// </summary>
        public static string GetMsmqQueueName(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":GetQueueName"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":GetQueueName");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetQueueName/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":GetQueueName", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":SendQueueName"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":SendQueueName");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "SendQueueName/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":SendQueueName", str, SystemConfigCacheTime);
                }
            }
            return str;
        }

        /// <summary>
        /// 获取MSMQ事务性：False,非事务性；True,事务性
        /// </summary>
        public static bool GetMsmqTransaction(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":GetQueueTransaction"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":GetQueueTransaction");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetQueueTransaction/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":GetQueueTransaction", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":SendQueueTransaction"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":SendQueueTransaction");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "SendQueueTransaction/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":SendQueueTransaction", str, SystemConfigCacheTime);
                }
            }
            if (str == "1")
                return true;
            else
                return false;
        }

        /// <summary>
        /// 获取MSMQ传输文件类型：ALL,所有文件文件；XML,XML文档；TXT,文本文件
        /// </summary>
        public static string GetMsmqTxtFile(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":GetQueueTxtFile"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":GetQueueTxtFile");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetQueueTxtFile/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":GetQueueTxtFile", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":SendQueueTxtFile"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":SendQueueTxtFile");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "SendQueueTxtFile/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":SendQueueTxtFile", str, SystemConfigCacheTime);
                }
            }
            return str;
        }

        /// <summary>
        /// 获取MSMQ传输文件编码类型
        /// </summary>
        public static string GetMsmqFileEncoding(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":GetQueueFileEncoding"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":GetQueueFileEncoding");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetQueueFileEncoding/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":GetQueueFileEncoding", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":SendQueueFileEncoding"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":SendQueueFileEncoding");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "SendQueueFileEncoding/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":SendQueueFileEncoding", str, SystemConfigCacheTime);
                }
            }
            return str;
        }
        #endregion

        #region ActiveMQ
        /// <summary>
        /// 新增ActiveMQ消息队列任务
        /// </summary>
        public static void AddActiveMq(string taskNo, string connectFactory, string destinationName, string transaction, bool isUpload)
        {
            if (!isUpload)
            {
                XmlHelper.AddNode(SYSTEM_CONFIGXML, "/GetActiveMqConnectFactory", taskNo.ToUpper(), connectFactory);
                XmlHelper.AddNode(SYSTEM_CONFIGXML, "/GetActiveMqDestination", taskNo.ToUpper(), destinationName);
                XmlHelper.AddNode(SYSTEM_CONFIGXML, "/GetActiveMqTransaction", taskNo.ToUpper(), transaction);
            }
            else
            {
                XmlHelper.AddNode(SYSTEM_CONFIGXML, "/SendActiveMqConnectFactory", taskNo.ToUpper(), connectFactory);
                XmlHelper.AddNode(SYSTEM_CONFIGXML, "/SendActiveMqDestination", taskNo.ToUpper(), destinationName);
                XmlHelper.AddNode(SYSTEM_CONFIGXML, "/SendActiveMqTransaction", taskNo.ToUpper(), transaction);
            }
        }

        /// <summary>
        ///设置ActiveMQ消息队列任务
        /// </summary>
        public static void SetActiveMq(string taskNo, string connectFactory, string destinationName, string transaction, bool isUpload)
        {
            if (!isUpload)
            {
                XmlHelper.setValue(SYSTEM_CONFIGXML, "GetActiveMqConnectFactory/", taskNo.ToUpper(), connectFactory);
                XmlHelper.setValue(SYSTEM_CONFIGXML, "GetActiveMqDestination/", taskNo.ToUpper(), destinationName);
                XmlHelper.setValue(SYSTEM_CONFIGXML, "GetActiveMqTransaction/", taskNo.ToUpper(), transaction);
            }
            else
            {
                XmlHelper.setValue(SYSTEM_CONFIGXML, "SendActiveMqConnectFactory/", taskNo.ToUpper(), connectFactory);
                XmlHelper.setValue(SYSTEM_CONFIGXML, "SendActiveMqDestination/", taskNo.ToUpper(), destinationName);
                XmlHelper.setValue(SYSTEM_CONFIGXML, "SendActiveMqTransaction/", taskNo.ToUpper(), transaction);
            }
        }

        /// <summary>
        /// 删除ActiveMQ设置
        /// </summary>
        public static void DelActiveMq(string taskNo, bool isUpload)
        {
            if (!isUpload)
            {
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetActiveMqConnectFactory", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetActiveMqDestination", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/GetActiveMqTransaction", taskNo.ToUpper());
            }
            else
            {
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/SendActiveMqConnectFactory", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/SendActiveMqDestination", taskNo.ToUpper());
                XmlHelper.DelNode(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "/SendActiveMqTransaction", taskNo.ToUpper());
            }
        }

        /// <summary>
        /// 获取ActiveMQ连接名称
        /// </summary>
        public static string GetActiveMqConnectFactory(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":GetActiveMqConnectFactory"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":GetActiveMqConnectFactory");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetActiveMqConnectFactory/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":GetActiveMqConnectFactory", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":SendActiveMqConnectFactory"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":SendActiveMqConnectFactory");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "SendActiveMqConnectFactory/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":SendActiveMqConnectFactory", str, SystemConfigCacheTime);
                }
            }
            return str;
        }

        /// <summary>
        /// 获取ActiveMQ队列名称
        /// </summary>
        public static string GetActiveMqDestinationName(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":GetActiveMqDestination"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":GetActiveMqDestination");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetActiveMqDestination/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":GetActiveMqDestination", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":SendActiveMqDestination"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":SendActiveMqDestination");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "SendActiveMqDestination/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":SendActiveMqDestination", str, SystemConfigCacheTime);
                }
            }
            return str;
        }

        /// <summary>
        /// 获取ActiveMQ事务性：False,非事务性；True,事务性
        /// </summary>
        public static bool GetActiveMqTransaction(string taskNo, bool isUpload)
        {
            string str = "";
            if (!isUpload)
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":GetActiveMqTransaction"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":GetActiveMqTransaction");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "GetActiveMqTransaction/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":GetActiveMqTransaction", str, SystemConfigCacheTime);
                }
            }
            else
            {
                if (SystemCache.CacheContainsUnexpiryTimeKey(_cache, taskNo + ":SendActiveMqTransaction"))
                    str = SystemCache.GetStringCacheValue(_cache, taskNo + ":SendActiveMqTransaction");
                else
                {
                    str = XmlHelper.getValue(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYSTEM_CONFIGXML), "SendActiveMqTransaction/", taskNo.ToUpper());
                    SystemCache.AddCache(_cache, taskNo + ":SendActiveMqTransaction", str, SystemConfigCacheTime);
                }
            }
            if (str == "1")
                return true;
            else
                return false;
        }
        #endregion
    }
}
