﻿using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using Common;
using Config;

namespace FileTransferService
{
    public partial class FileTransferService : ServiceBase
    {
        public FileTransferService()
        {
            InitializeComponent();
        }

        private List<Transfer> transfers = null;
        private List<Thread> threads = null;
        private Transfer transfer = null;
        private Thread thread = null;

        protected override void OnStart(string[] args)
        {
            try
            {
                if(this.transfers == null)
                    this.transfers = new List<Transfer>();
                if (this.transfers != null && this.transfers.Count > 0)
                    this.transfers.Clear();
                if(this.threads == null)
                    this.threads = new List<Thread>();
                if (this.threads != null && this.threads.Count > 0)
                    this.threads.Clear();
                //清除缓存
                SystemSet.ClearCache();
                //获取
                string[] corpNos = SystemSet.GetTaskNos.Split(new char[] { ',' });
                for (int i = 0; corpNos != null && i < corpNos.Length; i++)
                {
                    transfer = new Transfer();
                    transfer.ID = corpNos[i];
                    this.transfers.Add(transfer);

                    thread = new Thread(new ThreadStart(transfer.GetMessages));
                    thread.Name = "Thread_Get_" + corpNos[i];
                    this.threads.Add(thread);
                }
                //上传
                string[] uploadCorpNos = SystemSet.UploadTaskNos.Split(new char[] { ',' });
                for (int i = 0; uploadCorpNos != null && i < uploadCorpNos.Length; i++)
                {
                    transfer = new Transfer();
                    transfer.ID = uploadCorpNos[i];
                    this.transfers.Add(transfer);

                    thread = new Thread(new ThreadStart(transfer.UpMessages));
                    thread.Name = "Thread_Upload_" + uploadCorpNos[i];
                    this.threads.Add(thread);
                }

                foreach (Thread tr in this.threads)
                {
                    tr.Start();
                    Thread.Sleep(2);
                }
            }
            catch { }
        }

        protected override void OnStop()
        {
            try
            {
                if (this.transfers != null && this.transfers.Count > 0)
                {
                    foreach (Transfer tf in this.transfers.ToArray())
                        tf.IsStop = true;
                    Thread.Sleep(10);
                    for (int i = 0; this.threads != null && i < this.threads.Count; i++)
                    {
                        if (this.threads[i].ThreadState == System.Threading.ThreadState.Running)
                            this.threads[i].Join();
                        else if (this.threads[i].ThreadState == System.Threading.ThreadState.WaitSleepJoin)
                            this.threads[i].Abort();
                    }
                }
            }
            catch { }
        }
    }
}
