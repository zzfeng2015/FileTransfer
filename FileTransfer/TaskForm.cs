using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Config;

namespace FileTransfer
{
    public partial class TaskForm : Form
    {
        public TaskForm()
        {
            InitializeComponent();
        }

        private bool isEdit = false;
        /// <summary>
        /// 是否是编辑状态
        /// </summary>
        public bool IsEdit
        {
            set { this.isEdit = value; }
            get { return this.isEdit; }
        }

        private string taskNo;
        /// <summary>
        /// 任务代码
        /// </summary>
        public string TaskNo
        {
            set { this.taskNo = value; }
            get { return this.taskNo; }
        }

        private MainForm.OperateType opType;
        /// <summary>
        /// 操作类型
        /// </summary>
        public MainForm.OperateType OpType
        {
            set { this.opType = value; }
            get { return this.opType; }
        }

        #region 页面初始化
        private void TaskForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.rbLocal.Checked = true;
                this.pnLocal.Enabled = true;
                this.rbFtpDown.Checked = false;
                this.pnFtpDown.Enabled = false;
                this.rbEmailGet.Checked = false;
                this.pnEmailRec.Enabled = false;
                this.rbMsmqGet.Checked = false;
                this.pnMsmqGet.Enabled = false;
                this.rbActiveMqGet.Checked = false;
                this.pnActiveMqGet.Enabled = false;

                this.rbFtpUpload.Checked = true;
                this.pnFtpUpload.Enabled = true;
                this.rbEmailSend.Checked = false;
                this.pnEmailSend.Enabled = false;
                this.rbMsmqSend.Checked = false;
                this.pnMsmqSend.Enabled = false;
                this.rbActiveMqSend.Checked = false;
                this.pnActiveMqSend.Enabled = false;

                this.btnGetStop.Enabled = false;
                this.btnSendStop.Enabled = false;

                //MSMQ格式
                this.BindMsmqFileType();

                //修改
                if (this.isEdit)
                {
                    this.Text = "修改任务";
                    this.BindData();

                    if (this.opType == MainForm.OperateType.Get)//获取修改
                        this.tabControl.TabPages.Remove(this.tpSend);
                    else if (this.opType == MainForm.OperateType.Send)//上传修改
                        this.tabControl.TabPages.Remove(this.tpGet);
                }
                else
                    this.Text = "新增任务";
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "数据初始化出错！错误原因："+ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region 按钮事件
        private void btnRootView_Click(object sender, EventArgs e)
        {
            this.folderBrowser.ShowDialog(this);
            this.folderBrowser.Description = "请选择本地获取目录";
            this.tbLoaclRoot.Text = this.folderBrowser.SelectedPath;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region 获取数据校验
                string getSet = "";
                string getMode = "";
                string getThreadTime = "29";
                string getMsgSavePath = "";
                string getMsgSaveTimes = "";

                string localRoot = "";
                string localGetFilter = "";

                string ftpGetServer = "";
                int ftpGetPort = 21;
                string ftpGetRoot = "";
                string ftpGetUser = "";
                string ftpGetUserPass = "";
                string ftpGetFileEx = "";
                string ftpGetEncoding = "";
                string ftpGetFileRename = "";
                string ftpGetMode = "";

                string emailRecServer = "";
                int emailRecPort = 110;
                string emailRecUser = "";
                string emailRecUserPass = "";

                string getMsmqQueueName = "";
                string getMsmqTranscation = "";
                string getMsmqFileType = "";
                string getMsmqFileEncoding = "";

                string getActiveMqConnectName = "";
                string getActiveMqDestinationName = "";
                string getActiveMqTranscation = "";

                this.taskNo = this.tbTaskNo.Text.Trim();
                if (this.opType != MainForm.OperateType.Send && !string.IsNullOrEmpty(this.taskNo))
                {
                    try
                    {
                        getThreadTime = this.tbGetThreadTime.Text.Trim();
                        Convert.ToInt32(getThreadTime);
                    }
                    catch { }

                    localRoot = this.tbLoaclRoot.Text.Trim();
                    localGetFilter = this.tbLoaclRootFileEx.Text.Trim();

                    ftpGetServer = this.tbFtpDownServer.Text.Trim();
                    try
                    {
                        ftpGetPort = Convert.ToInt32(this.tbFtpDownPort.Text.Trim());
                    }
                    catch { }
                    ftpGetRoot = this.tbFtpDownRoot.Text.Trim();
                    ftpGetUser = this.tbFtpDownUser.Text.Trim();
                    ftpGetUserPass = this.tbFtpDownUserPass.Text.Trim();
                    if (this.chbFtpDownMode.Checked)
                        ftpGetMode = "1";
                    else
                        ftpGetMode = "0";
                    ftpGetFileEx = this.tbFtpDownFileEx.Text.Trim();
                    ftpGetEncoding = this.tbFtpDownEncoding.Text.Trim();
                    if (this.chbFtpDownFileRename.Checked)
                        ftpGetFileRename = "1";
                    else
                        ftpGetFileRename = "0";

                    emailRecServer = this.tbEmailRecServer.Text.Trim();
                    try
                    {
                        emailRecPort = Convert.ToInt32(this.tbEmailRecPort.Text.Trim());
                    }
                    catch { }
                    emailRecUser = this.tbEmailRecUser.Text.Trim();
                    emailRecUserPass = this.tbEmailRecUserPass.Text.Trim();

                    getMsgSavePath = this.tbGetMessageRoot.Text.Trim();
                    getMsgSaveTimes = this.tbGetSaveTimes.Text.Trim();

                    getMsmqQueueName = this.tbMsmqGetQueueName.Text.Trim();
                    if (this.chbMsmqGetTranscation.Checked)
                        getMsmqTranscation = "1";
                    else
                        getMsmqTranscation = "0";
                    getMsmqFileType = this.cbMsmqGetFileType.SelectedValue.ToString();
                    getMsmqFileEncoding = this.tbMsmqGetEncoding.Text.Trim();

                    getActiveMqConnectName = this.tbActiveMqGetConnectName.Text.Trim();
                    getActiveMqDestinationName = this.tbActiveMqGetDestinationName.Text.Trim();
                    if (this.chbActiveMqGetTranscation.Checked)
                        getActiveMqTranscation = "1";
                    else
                        getActiveMqTranscation = "0";

                    if (string.IsNullOrEmpty(this.taskNo))
                    {
                        MessageBox.Show(this, "任务代码不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.tbTaskNo.Focus();
                        return;
                    }
                    if (this.btnGetStart.Enabled)
                        getSet = "0";
                    else
                        getSet = "1";

                    if (getMsgSavePath == "")
                    {
                        MessageBox.Show(this, "获取本地保存目录不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.tbGetMessageRoot.Focus();
                        return;
                    }

                    if (this.rbLocal.Checked)
                    {
                        getMode = "0";
                        if (localRoot == "")
                        {
                            MessageBox.Show(this, "获取目录不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.tbLoaclRoot.Focus();
                            return;
                        }
                    }
                    else if (this.rbFtpDown.Checked)
                    {
                        getMode = "1";
                        if (ftpGetServer == "" || ftpGetUser == "" || ftpGetUserPass == "")
                        {
                            MessageBox.Show(this, "下载FTP服务器、用户和密码不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else if (this.rbEmailGet.Checked)
                    {
                        getMode = "2";
                        if (emailRecServer == "" || emailRecUser == "" || emailRecUserPass == "")
                        {
                            MessageBox.Show(this, "接收邮件服务器、收件人和密码不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else if (this.rbMsmqGet.Checked)
                    {
                        getMode = "3";
                        if (getMsmqQueueName == "")
                        {
                            MessageBox.Show(this, "MSMQ接收消息队列名不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.tbMsmqGetQueueName.Focus();
                            return;
                        }
                    }
                    else if (this.rbActiveMqGet.Checked)
                    {
                        getMode = "4";
                        if (getActiveMqConnectName == "" || getActiveMqDestinationName == "")
                        {
                            MessageBox.Show(this, "ActiveMQ接收连接名或目标名不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.tbActiveMqGetConnectName.Focus();
                            this.tbActiveMqGetDestinationName.Focus();
                            return;
                        }
                    }
                }
                #endregion

                #region 上传数据检验
                string sendCorpNo = "";
                string sendSet = "";
                string sendMode = "";
                string sendThreadTime = "31";
                string sendMsgPath = "";
                string sendMsgFilter = "";

                string ftpUploadServer = "";
                int ftpUploadPort = 21;
                string ftpUploadRoot = "";
                string ftpUploadUser = "";
                string ftpUploadUserPass = "";
                string ftpUploadMode = "";
                string ftpUploadEncoding = "";

                string emailSendServer = "";
                int emailSendPort = 25;
                string emailSendUser = "";
                string emailSendUserPass = "";
                string emailSendTo = "";

                string sendMsmqQueueName = "";
                string sendMsmqTranscation = "";
                string sendMsmqFileType = "";
                string sendMsmqFileEncoding = "";

                string sendActiveMqConnectName = "";
                string sendActiveMqDestinationName = "";
                string sendActiveMqTranscation = "";

                sendCorpNo = this.tbSendCorpNo.Text.Trim();
                if (this.opType != MainForm.OperateType.Get && !string.IsNullOrEmpty(sendCorpNo))
                {
                    if (this.btnSendStart.Enabled)
                        sendSet = "0";
                    else
                        sendSet = "1";
                    sendMsgPath = this.tbSendMessageRoot.Text.Trim();
                    sendMsgFilter = this.tbSendFileEx.Text.Trim();

                    try
                    {
                        sendThreadTime = this.tbSendThreadTime.Text.Trim();
                        Convert.ToInt32(sendThreadTime);
                    }
                    catch { }

                    ftpUploadServer = this.tbFtpUploadServer.Text.Trim();
                    try
                    {
                        ftpUploadPort = Convert.ToInt32(this.tbFtpUploadPort.Text.Trim());
                    }
                    catch { }
                    ftpUploadRoot = this.tbFtpUploadRoot.Text.Trim();
                    ftpUploadUser = this.tbFtpUploadUser.Text.Trim();
                    ftpUploadUserPass = this.tbFtpUploadUserPass.Text.Trim();
                    if (this.chbFtpUploadMode.Checked)
                        ftpUploadMode = "1";
                    else
                        ftpUploadMode = "0";
                    ftpUploadEncoding = this.tbFtpUploadEncoding.Text.Trim();

                    emailSendServer = this.tbEmailSendServer.Text.Trim();
                    try
                    {
                        emailSendPort = Convert.ToInt32(this.tbEmailSendPort.Text.Trim());
                    }
                    catch { }
                    emailSendUser = this.tbEmailSendUser.Text.Trim();
                    emailSendUserPass = this.tbEmailSendUserPass.Text.Trim();
                    emailSendTo = this.tbEmailSendTo.Text.Trim();

                    sendMsmqQueueName = this.tbMsmqSendQueueName.Text.Trim();
                    if (this.chbMsmqSendTranscation.Checked)
                        sendMsmqTranscation = "1";
                    else
                        sendMsmqTranscation = "0";
                    sendMsmqFileType = this.cbMsmqSendFileType.SelectedValue.ToString();
                    sendMsmqFileEncoding = this.tbMsmqSendEncoding.Text.Trim();

                    sendActiveMqConnectName = this.tbActiveMqSendConnectName.Text.Trim();
                    sendActiveMqDestinationName = this.tbActiveMqSendDestinationName.Text.Trim();
                    if (this.chbActiveMqSendTranscation.Checked)
                        sendActiveMqTranscation = "1";
                    else
                        sendActiveMqTranscation = "0";

                    if (string.IsNullOrEmpty(sendCorpNo))
                    {
                        MessageBox.Show(this, "任务代码不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.tbSendCorpNo.Focus();
                        return;
                    }
                    if (sendMsgPath == "")
                    {
                        MessageBox.Show(this, "本地发送目录不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.tbSendMessageRoot.Focus();
                        return;
                    }

                    if (this.rbFtpUpload.Checked)
                    {
                        sendMode = "0";
                        if (ftpUploadServer == "" || ftpUploadUser == "" || ftpUploadUserPass == "")
                        {
                            MessageBox.Show(this, "上传FTP服务器、用户和密码不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else if (this.rbEmailSend.Checked)
                    {
                        sendMode = "1";
                        if (emailSendServer == "" || emailSendUser == "" || emailSendUserPass == "" || emailSendTo == "")
                        {
                            MessageBox.Show(this, "发送邮件服务器、发件人、密码和收件人不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else if (this.rbMsmqSend.Checked)
                    {
                        sendMode = "2";
                        if (sendMsmqQueueName == "")
                        {
                            MessageBox.Show(this, "MSMQ发送消息队列名不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.tbMsmqSendQueueName.Focus();
                            return;
                        }
                    }
                    else if (this.rbActiveMqSend.Checked)
                    {
                        sendMode = "3";
                        if (sendActiveMqConnectName == "" || sendActiveMqDestinationName == "")
                        {
                            MessageBox.Show(this, "ActiveMQ发送连接名或目标名不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.tbActiveMqSendConnectName.Focus();
                            this.tbActiveMqSendDestinationName.Focus();
                            return;
                        }
                    }
                }
                #endregion

                if (!this.isEdit)//新增
                {
                    #region 下载
                    if (!string.IsNullOrEmpty(this.taskNo))
                    {
                        if (SystemSet.CheckTaskNo(this.taskNo, SystemSet.GetTaskNos))
                        {
                            MessageBox.Show(this, "获取任务代码已存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.tbTaskNo.Focus();
                            return;
                        }

                        SystemSet.AddTaskNo(this.taskNo, getSet, getThreadTime, getMode, getMsgSavePath, getMsgSaveTimes);

                        if (getMode == "0")
                            SystemSet.AddMsgGetRoot(this.taskNo, localRoot,localGetFilter);
                        else if (getMode == "1")
                            SystemSet.AddFtp(this.taskNo, ftpGetServer, ftpGetPort.ToString(), ftpGetUser, ftpGetUserPass, ftpGetRoot, ftpGetMode,ftpGetFileEx,ftpGetFileRename,ftpGetEncoding, false);
                        else if (getMode == "2")
                            SystemSet.AddEmail(this.taskNo, emailRecServer, emailRecPort.ToString(), emailRecUser, emailRecUserPass, "", false);
                        else if (getMode == "3")
                            SystemSet.AddMsmq(this.taskNo,getMsmqQueueName,getMsmqTranscation,getMsmqFileType,getMsmqFileEncoding, false);
                        else if (getMode == "4")
                            SystemSet.AddActiveMq(this.taskNo, getActiveMqConnectName,getActiveMqDestinationName, getActiveMqTranscation, false);

                        if (SystemSet.GetTaskNos == "")
                            SystemSet.GetTaskNos = taskNo;
                        else
                            SystemSet.GetTaskNos = SystemSet.GetTaskNos + "," + taskNo;
                    }
                    #endregion

                    #region 发送 
                    if (!string.IsNullOrEmpty(sendCorpNo))
                    {
                        if (SystemSet.CheckTaskNo(sendCorpNo, SystemSet.UploadTaskNos))
                        {
                            MessageBox.Show(this, "发送任务代码已存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.tbSendCorpNo.Focus();
                            return;
                        }

                        SystemSet.AddUploadTaskNo(sendCorpNo, sendSet, sendThreadTime, sendMode);
                        SystemSet.AddUploadLocalRoot(sendCorpNo, sendMsgPath,sendMsgFilter);

                        if (sendMode == "0")
                            SystemSet.AddFtp(sendCorpNo, ftpUploadServer, ftpUploadPort.ToString(), ftpUploadUser, ftpUploadUserPass, ftpUploadRoot, ftpUploadMode, "", "", ftpUploadEncoding, true);
                        else if (sendMode == "1")
                            SystemSet.AddEmail(sendCorpNo, emailSendServer, emailSendPort.ToString(), emailSendUser, emailSendUserPass, emailSendTo, true);
                        else if (sendMode == "2")
                            SystemSet.AddMsmq(sendCorpNo, sendMsmqQueueName, sendMsmqTranscation,sendMsmqFileType,sendMsmqFileEncoding, true);
                        else if (sendMode == "3")
                            SystemSet.AddActiveMq(sendCorpNo, sendActiveMqConnectName, sendActiveMqDestinationName, sendActiveMqTranscation, true);

                        if (SystemSet.UploadTaskNos == "")
                            SystemSet.UploadTaskNos = sendCorpNo;
                        else
                            SystemSet.UploadTaskNos = SystemSet.UploadTaskNos + "," + sendCorpNo;
                    }
                    #endregion
                }
                else//修改
                {
                    if (this.opType != MainForm.OperateType.Send && !string.IsNullOrEmpty(this.taskNo))
                    {
                        SystemSet.SetTaskNo(this.taskNo, getSet, getThreadTime, getMode, getMsgSavePath, getMsgSaveTimes);

                        if (getMode == "0")
                            SystemSet.SetMsgGetRoot(this.taskNo, localRoot,localGetFilter);
                        else if (getMode == "1")
                            SystemSet.SetFtp(this.taskNo, ftpGetServer, ftpGetPort.ToString(), ftpGetUser, ftpGetUserPass, ftpGetRoot, ftpGetMode, ftpGetFileEx, ftpGetFileRename, ftpGetEncoding, false);
                        else if (getMode == "2")
                            SystemSet.SetEmail(this.taskNo, emailRecServer, emailRecPort.ToString(), emailRecUser, emailRecUserPass, "", false);
                        else if (getMode == "3")
                            SystemSet.SetMsmq(this.taskNo, getMsmqQueueName, getMsmqTranscation, getMsmqFileType, getMsmqFileEncoding, false);
                        else if (getMode == "4")
                            SystemSet.SetActiveMq(this.taskNo, getActiveMqConnectName, getActiveMqDestinationName, getActiveMqTranscation, false);
                    }

                    if (this.opType != MainForm.OperateType.Get && !string.IsNullOrEmpty(sendCorpNo))
                    {
                        SystemSet.SetUploadTaskNo(sendCorpNo, sendSet, sendThreadTime, sendMode);
                        SystemSet.SetUploadLocalRoot(sendCorpNo, sendMsgPath,sendMsgFilter);

                        if (sendMode == "0")
                            SystemSet.SetFtp(sendCorpNo, ftpUploadServer, ftpUploadPort.ToString(), ftpUploadUser, ftpUploadUserPass, ftpUploadRoot, ftpUploadMode, "", "", ftpUploadEncoding, true);
                        else if (sendMode == "1")
                            SystemSet.SetEmail(sendCorpNo, emailSendServer, emailSendPort.ToString(), emailSendUser, emailSendUserPass, emailSendTo, true);
                        else if (sendMode == "2")
                            SystemSet.SetMsmq(sendCorpNo, sendMsmqQueueName, sendMsmqTranscation, sendMsmqFileType, sendMsmqFileEncoding, true);
                        else if (sendMode == "3")
                            SystemSet.SetActiveMq(sendCorpNo, sendActiveMqConnectName, sendActiveMqDestinationName, sendActiveMqTranscation, true);
                    }
                }

                MessageBox.Show(this, "保存成功！如果服务正在运行，请重启服务！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "数据操作出错！错误原因：" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "数据操作出错！错误原因：" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetRoot_Click(object sender, EventArgs e)
        {
            this.fbdGetRoot.ShowDialog(this);
            this.fbdGetRoot.Description = "请选择获取本地的保存目录";
            this.tbGetMessageRoot.Text = this.fbdGetRoot.SelectedPath;
        }

        private void btnSendRoot_Click(object sender, EventArgs e)
        {
            this.fdbSendRoot.ShowDialog(this);
            this.fdbSendRoot.Description = "请选择待发送的本地目录";
            this.tbSendMessageRoot.Text = this.fdbSendRoot.SelectedPath;
        }

        private void btnFrom_Click(object sender, EventArgs e)
        {
            //this.fbdTransferFrom.ShowDialog(this);
            //this.fbdTransferFrom.Description = "请选择待转换的本地存放目录";
            //this.tbTransferFromRoot.Text = this.fbdTransferFrom.SelectedPath;
        }

        private void btnTo_Click(object sender, EventArgs e)
        {
            //this.fbdTransferTo.ShowDialog(this);
            //this.fbdTransferTo.Description = "请选择转换后的本地存放目录";
            //this.tbTransferToRoot.Text = this.fbdTransferTo.SelectedPath;
        }

        private void btnGetStart_Click(object sender, EventArgs e)
        {
            Button bt = sender as Button;
            try
            {
                if (bt.Name == "btnGetStart")
                {
                    this.btnGetStart.Enabled = false;
                    this.btnGetStop.Enabled = true;
                    if (this.isEdit)
                        SystemSet.SetGetThreadSet(this.taskNo, "1");
                }
                else
                {
                    this.btnGetStart.Enabled = true;
                    this.btnGetStop.Enabled = false;
                    if (this.isEdit)
                        SystemSet.SetGetThreadSet(this.taskNo, "0");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("任务控制出错！错误原因：" + ex.Message);
            }
        }

        private void btnSendStart_Click(object sender, EventArgs e)
        {
            Button bt = sender as Button;
            try
            {
                if (bt.Name == "btnSendStart")
                {
                    this.btnSendStart.Enabled = false;
                    this.btnSendStop.Enabled = true;
                    if (this.isEdit)
                        SystemSet.SetUploadThreadSet(this.taskNo, "1");
                }
                else
                {
                    this.btnSendStart.Enabled = true;
                    this.btnSendStop.Enabled = false;
                    if (this.isEdit)
                        SystemSet.SetUploadThreadSet(this.taskNo, "0");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("任务控制出错！错误原因：" + ex.Message);
            }
        }
        #endregion

        #region 选择
        private void rbLocal_CheckedChanged(object sender, EventArgs e)
        {
            this.pnLocal.Enabled = this.rbLocal.Checked;
        }

        private void rbFtpDown_CheckedChanged(object sender, EventArgs e)
        {
            this.pnFtpDown.Enabled = this.rbFtpDown.Checked;
        }

        private void rbEmailGet_CheckedChanged(object sender, EventArgs e)
        {
            this.pnEmailRec.Enabled = this.rbEmailGet.Checked;
        }

        private void rbMsmqGet_CheckedChanged(object sender, EventArgs e)
        {
            this.pnMsmqGet.Enabled = this.rbMsmqGet.Checked;
        }

        private void rbFtpUpload_CheckedChanged(object sender, EventArgs e)
        {
            this.pnFtpUpload.Enabled = this.rbFtpUpload.Checked;
        }

        private void rbEmailSend_CheckedChanged(object sender, EventArgs e)
        {
            this.pnEmailSend.Enabled = this.rbEmailSend.Checked;
        }

        private void rbMsmqSend_CheckedChanged(object sender, EventArgs e)
        {
            this.pnMsmqSend.Enabled = this.rbMsmqSend.Checked;
        }

        private void rbActiveMqGet_CheckedChanged(object sender, EventArgs e)
        {
            this.pnActiveMqGet.Enabled = this.rbActiveMqGet.Checked;
        }

        private void rbActiveMqSend_CheckedChanged(object sender, EventArgs e)
        {
            this.pnActiveMqSend.Enabled = this.rbActiveMqSend.Checked;
        }
        #endregion

        #region 公共方法
        /// <summary>
        /// 绑定数据
        /// </summary>
        private void BindData()
        {
            #region 下载
            if (SystemSet.CheckTaskNo(this.taskNo, SystemSet.GetTaskNos))
            {
                this.tbTaskNo.Text = this.taskNo;
                this.tbTaskNo.Enabled = false;

                string getSet = SystemSet.GetGetThreadSet(this.taskNo);
                if (getSet == "1")
                {
                    this.btnGetStart.Enabled = false;
                    this.btnGetStop.Enabled = true;
                }
                else
                {
                    this.btnGetStart.Enabled = true;
                    this.btnGetStop.Enabled = false;
                }
                this.tbGetThreadTime.Text = SystemSet.GetGetThreadTime(this.taskNo).ToString();
                this.tbGetMessageRoot.Text = SystemSet.GetGetMsgSavePath(this.taskNo);
                this.tbGetSaveTimes.Text = SystemSet.GetGetMsgSaveTimes(this.taskNo).ToString();

                string getMode = SystemSet.GetGetMsgMode(this.taskNo);
                if (getMode == "0")
                {
                    this.rbLocal.Checked = true;
                    this.pnLocal.Enabled = true;
                    this.rbFtpDown.Checked = false;
                    this.pnFtpDown.Enabled = false;
                    this.rbEmailGet.Checked = false;
                    this.pnEmailRec.Enabled = false;
                    this.rbMsmqGet.Checked = false;
                    this.pnMsmqGet.Enabled = false;
                    this.rbActiveMqGet.Checked = false;
                    this.pnActiveMqGet.Enabled = false;
                }
                else if (getMode == "1")
                {
                    this.rbLocal.Checked = false;
                    this.pnLocal.Enabled = false;
                    this.rbFtpDown.Checked = true;
                    this.pnFtpDown.Enabled = true;
                    this.rbEmailGet.Checked = false;
                    this.pnEmailRec.Enabled = false;
                    this.rbMsmqGet.Checked = false;
                    this.pnMsmqGet.Enabled = false;
                    this.rbActiveMqGet.Checked = false;
                    this.pnActiveMqGet.Enabled = false;
                }
                else if (getMode == "2")
                {
                    this.rbLocal.Checked = false;
                    this.pnLocal.Enabled = false;
                    this.rbFtpDown.Checked = false;
                    this.pnFtpDown.Enabled = false;
                    this.rbEmailGet.Checked = true;
                    this.pnEmailRec.Enabled = true;
                    this.rbMsmqGet.Checked = false;
                    this.pnMsmqGet.Enabled = false;
                    this.rbActiveMqGet.Checked = false;
                    this.pnActiveMqGet.Enabled = false;
                }
                else if (getMode == "3")
                {
                    this.rbLocal.Checked = false;
                    this.pnLocal.Enabled = false;
                    this.rbFtpDown.Checked = false;
                    this.pnFtpDown.Enabled = false;
                    this.rbEmailGet.Checked = false;
                    this.pnEmailRec.Enabled = false;
                    this.rbMsmqGet.Checked = true;
                    this.pnMsmqGet.Enabled = true;
                    this.rbActiveMqGet.Checked = false;
                    this.pnActiveMqGet.Enabled = false;
                }
                else if (getMode == "4")
                {
                    this.rbLocal.Checked = false;
                    this.pnLocal.Enabled = false;
                    this.rbFtpDown.Checked = false;
                    this.pnFtpDown.Enabled = false;
                    this.rbEmailGet.Checked = false;
                    this.pnEmailRec.Enabled = false;
                    this.rbMsmqGet.Checked = false;
                    this.pnMsmqGet.Enabled = false;
                    this.rbActiveMqGet.Checked = true;
                    this.pnActiveMqGet.Enabled = true;
                }

                this.tbLoaclRoot.Text = SystemSet.GetGetMsgRoot(this.taskNo);
                this.tbLoaclRootFileEx.Text = SystemSet.GetGetMsgFilter(this.taskNo);

                this.tbFtpDownServer.Text = SystemSet.GetFtpServer(this.taskNo, false);
                this.tbFtpDownPort.Text = SystemSet.GetFtpPort(this.taskNo, false).ToString();
                this.tbFtpDownRoot.Text = SystemSet.GetFtpRoot(this.taskNo, false);
                this.tbFtpDownUser.Text = SystemSet.GetFtpUser(this.taskNo, false);
                this.tbFtpDownUserPass.Text = SystemSet.GetFtpUserPass(taskNo, false);
                this.chbFtpDownMode.Checked = SystemSet.GetFtpMode(this.taskNo, false);

                this.tbEmailRecServer.Text = SystemSet.GetEmailServer(this.taskNo, false);
                this.tbEmailRecPort.Text = SystemSet.GetEmailPort(this.taskNo, false).ToString();
                this.tbEmailRecUser.Text = SystemSet.GetEmailUser(this.taskNo, false);
                this.tbEmailRecUserPass.Text = SystemSet.GetEmailUserPass(this.taskNo, false);

                this.tbMsmqGetQueueName.Text = SystemSet.GetMsmqQueueName(this.taskNo, false);
                this.chbMsmqGetTranscation.Checked = SystemSet.GetMsmqTransaction(this.taskNo, false);

                string msmqGetFileType = SystemSet.GetMsmqTxtFile(this.taskNo, false);
                if(msmqGetFileType == "ALL")
                    this.cbMsmqGetFileType.SelectedIndex = 0;
                else if(msmqGetFileType == "XML")
                    this.cbMsmqGetFileType.SelectedIndex = 1;
                 else if(msmqGetFileType == "TXT")
                    this.cbMsmqGetFileType.SelectedIndex = 2;
                this.tbMsmqGetEncoding.Text = SystemSet.GetMsmqFileEncoding(this.taskNo, false);

                this.tbActiveMqGetConnectName.Text = SystemSet.GetActiveMqConnectFactory(this.taskNo, false);
                this.tbActiveMqGetDestinationName.Text = SystemSet.GetActiveMqDestinationName(this.taskNo, false);
                this.chbActiveMqGetTranscation.Checked = SystemSet.GetActiveMqTransaction(this.taskNo, false);
            }
            #endregion

            #region 上传
            if (SystemSet.CheckTaskNo(this.taskNo, SystemSet.UploadTaskNos))
            {
                this.tbSendCorpNo.Text = this.taskNo;
                this.tbSendCorpNo.Enabled = false;
                this.tbSendThreadTime.Text = SystemSet.GetUploadThreadSetTime(this.taskNo).ToString();

                string uploadSet = SystemSet.GetUploadThreadSet(this.taskNo);
                if (uploadSet == "1")
                {
                    this.btnSendStart.Enabled = false;
                    this.btnSendStop.Enabled = true;
                }
                else
                {
                    this.btnSendStart.Enabled = true;
                    this.btnSendStop.Enabled = false;
                }

                this.tbSendMessageRoot.Text = SystemSet.GetUploadLoaclRoot(this.taskNo);
                this.tbSendFileEx.Text = SystemSet.GetUploadLoaclRootFilter(this.taskNo);

                string uploadMode = SystemSet.GetUoloadMsgMode(this.taskNo);
                if (uploadMode == "0")
                {
                    this.rbFtpUpload.Checked = true;
                    this.pnFtpUpload.Enabled = true;
                    this.rbEmailSend.Checked = false;
                    this.pnEmailSend.Enabled = false;
                    this.rbMsmqSend.Checked = false;
                    this.pnMsmqSend.Enabled = false;
                    this.rbActiveMqSend.Checked = false;
                    this.pnActiveMqSend.Enabled = false;
                }
                else if (uploadMode == "1")
                {
                    this.rbFtpUpload.Checked = false;
                    this.pnFtpUpload.Enabled = false;
                    this.rbEmailSend.Checked = true;
                    this.pnEmailSend.Enabled = true;
                    this.rbMsmqSend.Checked = false;
                    this.pnMsmqSend.Enabled = false;
                    this.rbActiveMqSend.Checked = false;
                    this.pnActiveMqSend.Enabled = false;
                }
                else if (uploadMode == "2")
                {
                    this.rbFtpUpload.Checked = false;
                    this.pnFtpUpload.Enabled = false;
                    this.rbEmailSend.Checked = false;
                    this.pnEmailSend.Enabled = false;
                    this.rbMsmqSend.Checked = true;
                    this.pnMsmqSend.Enabled = true;
                    this.rbActiveMqSend.Checked = false;
                    this.pnActiveMqSend.Enabled = false;
                }
                else if (uploadMode == "3")
                {
                    this.rbFtpUpload.Checked = false;
                    this.pnFtpUpload.Enabled = false;
                    this.rbEmailSend.Checked = false;
                    this.pnEmailSend.Enabled = false;
                    this.rbMsmqSend.Checked = false;
                    this.pnMsmqSend.Enabled = false;
                    this.rbActiveMqSend.Checked = true;
                    this.pnActiveMqSend.Enabled = true;
                }

                this.tbFtpUploadServer.Text = SystemSet.GetFtpServer(this.taskNo, true);
                this.tbFtpUploadPort.Text = SystemSet.GetFtpPort(this.taskNo, true).ToString();
                this.tbFtpUploadRoot.Text = SystemSet.GetFtpRoot(this.taskNo, true);
                this.tbFtpUploadUser.Text = SystemSet.GetFtpUser(this.taskNo, true);
                this.tbFtpUploadUserPass.Text = SystemSet.GetFtpUserPass(taskNo, true);
                this.chbFtpUploadMode.Checked = SystemSet.GetFtpMode(this.taskNo, true);

                this.tbEmailSendServer.Text = SystemSet.GetEmailServer(this.taskNo, true);
                this.tbEmailSendPort.Text = SystemSet.GetEmailPort(this.taskNo, true).ToString();
                this.tbEmailSendUser.Text = SystemSet.GetEmailUser(this.taskNo, true);
                this.tbEmailSendUserPass.Text = SystemSet.GetEmailUserPass(this.taskNo, true);
                this.tbEmailSendTo.Text = SystemSet.GetEmailTo(this.taskNo, true);

                this.tbMsmqSendQueueName.Text = SystemSet.GetMsmqQueueName(this.taskNo, true);
                this.chbMsmqSendTranscation.Checked = SystemSet.GetMsmqTransaction(this.taskNo, true);

                string msmqSendFileType = SystemSet.GetMsmqTxtFile(this.taskNo, true);
                if (msmqSendFileType == "ALL")
                    this.cbMsmqSendFileType.SelectedIndex = 0;
                else if (msmqSendFileType == "XML")
                    this.cbMsmqSendFileType.SelectedIndex = 1;
                else if (msmqSendFileType == "TXT")
                    this.cbMsmqSendFileType.SelectedIndex = 2;
                this.tbMsmqSendEncoding.Text = SystemSet.GetMsmqFileEncoding(this.taskNo, true);

                this.tbActiveMqSendConnectName.Text = SystemSet.GetActiveMqConnectFactory(this.taskNo, true);
                this.tbActiveMqSendDestinationName.Text = SystemSet.GetActiveMqDestinationName(this.taskNo, true);
                this.chbActiveMqSendTranscation.Checked = SystemSet.GetActiveMqTransaction(this.taskNo, true);
            }
            #endregion 
        }

        /// <summary>
        /// MSMQ传输文件类型
        /// </summary>
        private void BindMsmqFileType()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Value");

            dt.Rows.Add(new object[] { "任意文件", "ALL"});
            dt.Rows.Add(new object[] { "XML文档", "XML" });
            dt.Rows.Add(new object[] { "文本文件", "TXT" });

            this.cbMsmqGetFileType.DataSource = dt;
            this.cbMsmqGetFileType.DisplayMember = "Name";
            this.cbMsmqGetFileType.ValueMember = "Value";

            this.cbMsmqSendFileType.DataSource = dt.Copy(); ;
            this.cbMsmqSendFileType.DisplayMember = "Name";
            this.cbMsmqSendFileType.ValueMember = "Value";
        }
        #endregion
    }
}
